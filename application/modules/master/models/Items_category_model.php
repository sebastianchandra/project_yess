<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Items_category_model extends CI_Model
{

    /* START PRIVATE VARIABLES */
     private $myDb = 'db_yess';
     private $myTable = 'mt_category';
     private $categoryId;
     private $categoryCode;
     private $categoryName;
     private $remarks;
     private $picInput;
     private $inputTime;
     private $picEdit;
     private $editTime;
     /* END PRIVATE VARIABLES */
     /* START CONSTRUCTOR */
     public function __construct()
     {
        parent::__construct();
          $this->categoryId = 0;
          $this->categoryCode = '';
          $this->categoryName = '';
          $this->remarks = '';
          $this->picInput = '';
          $this->inputTime = '0000-00-00 00:00:00';
          $this->picEdit = '';
          $this->editTime = '0000-00-00 00:00:00';
     }
     /* END CONSTRUCTOR */
     
     /* START GENERATE SETTER AND GETTER */
     public function setCategoryId($aCategoryId)
     {
        $this->categoryId = $this->db->escape_str($aCategoryId);
     }
     public function getCategoryId()
     {
        return $this->categoryId;
     }
     public function setCategoryCode($aCategoryCode)
     {
        $this->categoryCode = $this->db->escape_str($aCategoryCode);
     }
     public function getCategoryCode()
     {
        return $this->categoryCode;
     }
     public function setCategoryName($aCategoryName)
     {
        $this->categoryName = $this->db->escape_str($aCategoryName);
     }
     public function getCategoryName()
     {
        return $this->categoryName;
     }
     public function setRemarks($aRemarks)
     {
        $this->remarks = $this->db->escape_str($aRemarks);
     }
     public function getRemarks()
     {
        return $this->remarks;
     }
     public function setPicInput($aPicInput)
     {
        $this->picInput = $this->db->escape_str($aPicInput);
     }
     public function getPicInput()
     {
        return $this->picInput;
     }
     public function setInputTime($aInputTime)
     {
        $this->inputTime = $this->db->escape_str($aInputTime);
     }
     public function getInputTime()
     {
        return $this->inputTime;
     }
     public function setPicEdit($aPicEdit)
     {
        $this->picEdit = $this->db->escape_str($aPicEdit);
     }
     public function getPicEdit()
     {
        return $this->picEdit;
     }
     public function setEditTime($aEditTime)
     {
        $this->editTime = $this->db->escape_str($aEditTime);
     }
     public function getEditTime()
     {
        return $this->editTime;
     }
     /* END GENERATE SETTER AND GETTER */
     /* START INSERT */
     public function insert()
     {
        if($this->categoryId =='' || $this->categoryId == NULL )
        {
            $this->categoryId = 0;
        }
        if($this->categoryCode =='' || $this->categoryCode == NULL )
        {
            $this->categoryCode = '';
        }
        if($this->categoryName =='' || $this->categoryName == NULL )
        {
            $this->categoryName = '';
        }
        if($this->remarks =='' || $this->remarks == NULL )
        {
            $this->remarks = '';
        }
        if($this->picInput =='' || $this->picInput == NULL )
        {
            $this->picInput = '';
        }
        if($this->inputTime =='' || $this->inputTime == NULL )
        {
            $this->inputTime = '0000-00-00 00:00:00';
        }
        if($this->picEdit =='' || $this->picEdit == NULL )
        {
            $this->picEdit = '';
        }
        if($this->editTime =='' || $this->editTime == NULL )
        {
            $this->editTime = '0000-00-00 00:00:00';
        }
        
        $stQuery  = 'INSERT INTO '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= '( '; 
        $stQuery .=   'category_id,'; 
        $stQuery .=   'category_code,'; 
        $stQuery .=   'category_name,'; 
        $stQuery .=   'remarks,'; 
        $stQuery .=   'is_active,'; 
        $stQuery .=   'pic_input,'; 
        $stQuery .=   'input_time'; 
        $stQuery .= ') '; 
        $stQuery .= 'VALUES '; 
        $stQuery .= '( '; 
        $stQuery .=   $this->db->escape_str($this->categoryId).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->categoryCode).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->categoryName).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->remarks).'",'; 
        $stQuery .=   '"1",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->picInput).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->inputTime).'"'; 
        $stQuery .= '); '; 

        $query  = $this->db->query($stQuery); 

        if ($query === false){
            return "ERROR INSERTT";
        }else{
            return $query; 
        }
     }
     /* END INSERT */
     /* START UPDATE */
     public function update($id)
     {
        if($this->categoryId =='' || $this->categoryId == NULL )
        {
            $this->categoryId = 0;
        }
        if($this->categoryCode =='' || $this->categoryCode == NULL )
        {
            $this->categoryCode = '';
        }
        if($this->categoryName =='' || $this->categoryName == NULL )
        {
            $this->categoryName = '';
        }
        if($this->remarks =='' || $this->remarks == NULL )
        {
            $this->remarks = '';
        }
        if($this->picInput =='' || $this->picInput == NULL )
        {
            $this->picInput = '';
        }
        if($this->inputTime =='' || $this->inputTime == NULL )
        {
            $this->inputTime = '0000-00-00 00:00:00';
        }
        if($this->picEdit =='' || $this->picEdit == NULL )
        {
            $this->picEdit = '';
        }
        if($this->editTime =='' || $this->editTime == NULL )
        {
            $this->editTime = '0000-00-00 00:00:00';
        }
        
        $stQuery  = 'UPDATE '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= 'SET '; 
        $stQuery .=   'category_code ="'.$this->db->escape_str($this->categoryCode).'",'; 
        $stQuery .=   'category_name ="'.$this->db->escape_str($this->categoryName).'",'; 
        $stQuery .=   'remarks ="'.$this->db->escape_str($this->remarks).'",'; 
        $stQuery .=   'pic_edit ="'.$this->db->escape_str($this->picEdit).'",'; 
        $stQuery .=   'edit_time ="'.$this->db->escape_str($this->editTime).'" '; 
        $stQuery .= 'WHERE '; 
        $stQuery .=   'category_id = '.$this->db->escape_str($id).''; 
        $query  = $this->db->query($stQuery); 
        if ($query === false){
            return "ERROR INSERTT";
        }else{
            return $query; 
        }
     }

	function get_category()
    {
        $sql ='SELECT * FROM mt_category WHERE is_active=1 order by category_id DESC';
        $query = $this->db->query($sql);
        return $query->result();
    }

    function max_id(){
        $sql        = 'SELECT IFNULL(MAX(category_id),0)+1 id FROM mt_category';
        return $this->db->query($sql)->row();
    }

    function detail_category($id){
        $query = $this->db->query("SELECT * FROM mt_category WHERE category_id='".$id."'")->row();
        return $query;
    }

    function act_delete_js(){
        $sql = "UPDATE mt_category SET is_active='0', pic_edit = '".$this->current_user['user_id']."', edit_time = '".dbnow()."' WHERE category_id = '".$this->input->post('id')."'";
        //test($sql,1);
        $query = $this->db->query($sql);
        return $query;
    }

    function get_category_detail($id){
        $sql = "SELECT category_id FROM br_items_category WHERE items_id='".$id."'";
        return $this->db->query($sql)->result();
    }

}	