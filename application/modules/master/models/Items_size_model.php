<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Items_size_model extends CI_Model
{

    private $myDb = 'db_yess';
     private $myTable = 'mt_size';
     private $sizeId;
     private $sizeCode;
     private $sizeTitle;
     private $sizeContent;
     private $weight;
     private $price;
     private $discPrice;
     private $quantity;
     private $picInput;
     private $inputTime;
     private $picEdit;
     private $editTime;
     /* END PRIVATE VARIABLES */
     /* START CONSTRUCTOR */
     public function __construct()
     {
        parent::__construct();
          $this->sizeId = 0;
          $this->sizeCode = '';
          $this->sizeTitle = '';
          $this->sizeContent = '';
          $this->weight = 0;
          $this->price = 0;
          $this->discPrice = 0;
          $this->quantity = 0;
          $this->picInput = '';
          $this->inputTime = '0000-00-00 00:00:00';
          $this->picEdit = '';
          $this->editTime = '0000-00-00 00:00:00';
     }
     /* END CONSTRUCTOR */
     
     /* START GENERATE SETTER AND GETTER */
     public function setSizeId($aSizeId)
     {
        $this->sizeId = $this->db->escape_str($aSizeId);
     }
     public function getSizeId()
     {
        return $this->sizeId;
     }
     public function setSizeCode($aSizeCode)
     {
        $this->sizeCode = $this->db->escape_str($aSizeCode);
     }
     public function getSizeCode()
     {
        return $this->sizeCode;
     }
     public function setSizeTitle($aSizeTitle)
     {
        $this->sizeTitle = $this->db->escape_str($aSizeTitle);
     }
     public function getSizeTitle()
     {
        return $this->sizeTitle;
     }
     public function setSizeContent($aSizeContent)
     {
        $this->sizeContent = $this->db->escape_str($aSizeContent);
     }
     public function getSizeContent()
     {
        return $this->sizeContent;
     }
     public function setWeight($aWeight)
     {
        $this->weight = $this->db->escape_str($aWeight);
     }
     public function getWeight()
     {
        return $this->weight;
     }
     public function setPrice($aPrice)
     {
        $this->price = $this->db->escape_str($aPrice);
     }
     public function getPrice()
     {
        return $this->price;
     }
     public function setDiscPrice($aDiscPrice)
     {
        $this->discPrice = $this->db->escape_str($aDiscPrice);
     }
     public function getDiscPrice()
     {
        return $this->discPrice;
     }
     public function setQuantity($aQuantity)
     {
        $this->quantity = $this->db->escape_str($aQuantity);
     }
     public function getQuantity()
     {
        return $this->quantity;
     }
     public function setPicInput($aPicInput)
     {
        $this->picInput = $this->db->escape_str($aPicInput);
     }
     public function getPicInput()
     {
        return $this->picInput;
     }
     public function setInputTime($aInputTime)
     {
        $this->inputTime = $this->db->escape_str($aInputTime);
     }
     public function getInputTime()
     {
        return $this->inputTime;
     }
     public function setPicEdit($aPicEdit)
     {
        $this->picEdit = $this->db->escape_str($aPicEdit);
     }
     public function getPicEdit()
     {
        return $this->picEdit;
     }
     public function setEditTime($aEditTime)
     {
        $this->editTime = $this->db->escape_str($aEditTime);
     }
     public function getEditTime()
     {
        return $this->editTime;
     }
     /* END GENERATE SETTER AND GETTER */
     /* START INSERT */
     public function insert()
     {
        if($this->sizeId =='' || $this->sizeId == NULL )
        {
            $this->sizeId = 0;
        }
        if($this->sizeCode =='' || $this->sizeCode == NULL )
        {
            $this->sizeCode = '';
        }
        if($this->sizeTitle =='' || $this->sizeTitle == NULL )
        {
            $this->sizeTitle = '';
        }
        if($this->sizeContent =='' || $this->sizeContent == NULL )
        {
            $this->sizeContent = '';
        }
        if($this->weight =='' || $this->weight == NULL )
        {
            $this->weight = 0;
        }
        if($this->price =='' || $this->price == NULL )
        {
            $this->price = 0;
        }
        if($this->discPrice =='' || $this->discPrice == NULL )
        {
            $this->discPrice = 0;
        }
        if($this->quantity =='' || $this->quantity == NULL )
        {
            $this->quantity = 0;
        }
        if($this->picInput =='' || $this->picInput == NULL )
        {
            $this->picInput = '';
        }
        if($this->inputTime =='' || $this->inputTime == NULL )
        {
            $this->inputTime = '0000-00-00 00:00:00';
        }
        if($this->picEdit =='' || $this->picEdit == NULL )
        {
            $this->picEdit = '';
        }
        if($this->editTime =='' || $this->editTime == NULL )
        {
            $this->editTime = '0000-00-00 00:00:00';
        }
        
        $stQuery  = 'INSERT INTO '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= '( '; 
        $stQuery .=   'size_id,'; 
        $stQuery .=   'size_code,'; 
        $stQuery .=   'size_title,'; 
        // $stQuery .=   'size_content,'; 
        $stQuery .=   'weight,'; 
        $stQuery .=   'is_active,'; 
        $stQuery .=   'pic_input,'; 
        $stQuery .=   'input_time'; 
        // $stQuery .=   'pic_edit,'; 
        // $stQuery .=   'edit_time'; 
        $stQuery .= ') '; 
        $stQuery .= 'VALUES '; 
        $stQuery .= '( '; 
        $stQuery .=   $this->db->escape_str($this->sizeId).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->sizeCode).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->sizeTitle).'",'; 
        // $stQuery .=   $this->db->escape_str($this->sizeContent).','; 
        $stQuery .=   $this->db->escape_str($this->weight).','; 
        $stQuery .=   '1,'; 
        $stQuery .=   '"'.$this->db->escape_str($this->picInput).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->inputTime).'"'; 
        // $stQuery .=   '"'.$this->db->escape_str($this->picEdit).'",'; 
        // $stQuery .=   '"'.$this->db->escape_str($this->editTime).'"'; 
        $stQuery .= '); '; 
        $this->db->query($stQuery); 
     }
     /* END INSERT */
     /* START UPDATE */
     public function update($id)
     {
        if($this->sizeId =='' || $this->sizeId == NULL )
        {
            $this->sizeId = 0;
        }
        if($this->sizeCode =='' || $this->sizeCode == NULL )
        {
            $this->sizeCode = '';
        }
        if($this->sizeTitle =='' || $this->sizeTitle == NULL )
        {
            $this->sizeTitle = '';
        }
        if($this->sizeContent =='' || $this->sizeContent == NULL )
        {
            $this->sizeContent = '';
        }
        if($this->weight =='' || $this->weight == NULL )
        {
            $this->weight = 0;
        }
        if($this->price =='' || $this->price == NULL )
        {
            $this->price = 0;
        }
        if($this->discPrice =='' || $this->discPrice == NULL )
        {
            $this->discPrice = 0;
        }
        if($this->quantity =='' || $this->quantity == NULL )
        {
            $this->quantity = 0;
        }
        if($this->picInput =='' || $this->picInput == NULL )
        {
            $this->picInput = '';
        }
        if($this->inputTime =='' || $this->inputTime == NULL )
        {
            $this->inputTime = '0000-00-00 00:00:00';
        }
        if($this->picEdit =='' || $this->picEdit == NULL )
        {
            $this->picEdit = '';
        }
        if($this->editTime =='' || $this->editTime == NULL )
        {
            $this->editTime = '0000-00-00 00:00:00';
        }
        
        $stQuery  = 'UPDATE '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= 'SET '; 
        $stQuery .=   'size_code ="'.$this->db->escape_str($this->sizeCode).'",'; 
        $stQuery .=   'size_title ="'.$this->db->escape_str($this->sizeTitle).'",'; 
        // $stQuery .=   'size_content ='.$this->db->escape_str($this->sizeContent).','; 
        $stQuery .=   'weight ='.$this->db->escape_str($this->weight).','; 
        $stQuery .=   'pic_edit ="'.$this->db->escape_str($this->picEdit).'",'; 
        $stQuery .=   'edit_time ="'.$this->db->escape_str($this->editTime).'" '; 
        $stQuery .= 'WHERE '; 
        $stQuery .=   'size_id = '.$this->db->escape_str($id).''; 
        $this->db->query($stQuery); 
     }
     /* END UPDATE */

	function get_size()
    {
        $sql ='SELECT * FROM mt_size WHERE is_active=1 order by size_id DESC';
        $query = $this->db->query($sql);
        return $query->result();
    }

    function max_id(){
        $sql        = 'SELECT IFNULL(MAX(size_id),0)+1 id FROM mt_size';
        return $this->db->query($sql)->row();
    }

    function detail_size($id){
        $query = $this->db->query("SELECT * FROM mt_size WHERE size_id='".$id."'")->row();
        return $query;
    }

    function act_delete_js(){
        $sql = "UPDATE mt_size SET is_active='0', pic_edit = '".$this->current_user['user_id']."', edit_time = '".dbnow()."' WHERE size_id = '".$this->input->post('id')."'";
        //test($sql,1);
        $query = $this->db->query($sql);
        return $query;
    }

}	