<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Items_color_model extends CI_Model
{

    /* START PRIVATE VARIABLES */
     private $myDb = 'db_yess';
     private $myTable = 'mt_color';
     private $colorId;
     private $colorCode;
     private $colorName;
     /* END PRIVATE VARIABLES */
     /* START CONSTRUCTOR */
     public function __construct()
     {
        parent::__construct();
          $this->colorId = 0;
          $this->colorCode = '';
          $this->colorName = '';
     }
     /* END CONSTRUCTOR */
     
     /* START GENERATE SETTER AND GETTER */
     public function setColorId($aColorId)
     {
        $this->colorId = $this->db->escape_str($aColorId);
     }
     public function getColorId()
     {
        return $this->colorId;
     }
     public function setColorCode($aColorCode)
     {
        $this->colorCode = $this->db->escape_str($aColorCode);
     }
     public function getColorCode()
     {
        return $this->colorCode;
     }
     public function setColorName($aColorName)
     {
        $this->colorName = $this->db->escape_str($aColorName);
     }
     public function getColorName()
     {
        return $this->colorName;
     }
     /* END GENERATE SETTER AND GETTER */
     /* START INSERT */
     public function insert()
     {
        if($this->colorId =='' || $this->colorId == NULL )
        {
            $this->colorId = 0;
        }
        if($this->colorCode =='' || $this->colorCode == NULL )
        {
            $this->colorCode = '';
        }
        if($this->colorName =='' || $this->colorName == NULL )
        {
            $this->colorName = '';
        }
        
        $stQuery  = 'INSERT INTO '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= '( '; 
        $stQuery .=   'color_id,'; 
        $stQuery .=   'color_code,'; 
        $stQuery .=   'color_name,'; 
        $stQuery .=   'is_active,'; 
        $stQuery .=   'pic_input,'; 
        $stQuery .=   'input_time'; 
        $stQuery .= ') '; 
        $stQuery .= 'VALUES '; 
        $stQuery .= '( '; 
        $stQuery .=   $this->db->escape_str($this->colorId).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->colorCode).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->colorName).'",'; 
        $stQuery .=   '1,'; 
        $stQuery .=   '"'.$this->db->escape_str($this->current_user['user_id']).'",'; 
        $stQuery .=   '"'.$this->db->escape_str(dbnow()).'"'; 
        $stQuery .= '); '; 
        $this->db->query($stQuery); 
     }
     /* END INSERT */
     /* START UPDATE */
     public function update($id)
     {
        if($this->colorId =='' || $this->colorId == NULL )
        {
            $this->colorId = 0;
        }
        if($this->colorCode =='' || $this->colorCode == NULL )
        {
            $this->colorCode = '';
        }
        if($this->colorName =='' || $this->colorName == NULL )
        {
            $this->colorName = '';
        }
        
        $stQuery  = 'UPDATE '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= 'SET '; 
        $stQuery .=   'color_code ="'.$this->db->escape_str($this->colorCode).'",'; 
        $stQuery .=   'color_name ="'.$this->db->escape_str($this->colorName).'" '; 
        $stQuery .=   'pic_edit ="'.$this->db->escape_str($this->current_user['user_id']).'" '; 
        $stQuery .=   'edit_time ="'.$this->db->escape_str(dbnow()).'" '; 
        $stQuery .= 'WHERE '; 
        $stQuery .=   'color_id = '.$this->db->escape_str($id).''; 
        $this->db->query($stQuery); 
     }
     /* END UPDATE */
     

	function get_color()
    {
        $sql ='SELECT * FROM mt_color WHERE is_active=1 order by color_id DESC';
        $query = $this->db->query($sql);
        return $query->result();
    }

    function max_id(){
        $sql        = 'SELECT IFNULL(MAX(color_id),0)+1 id FROM mt_color';
        return $this->db->query($sql)->row();
    }

    function detail_color($id){
        $query = $this->db->query("SELECT * FROM mt_color WHERE color_id='".$id."'")->row();
        return $query;
    }

    function act_delete_js(){
        $sql = "UPDATE mt_color SET is_active='0', pic_edit = '".$this->current_user['user_id']."', edit_time = '".dbnow()."' WHERE color_id = '".$this->input->post('id')."'";
        //test($sql,1);
        $query = $this->db->query($sql);
        return $query;
    }

}	