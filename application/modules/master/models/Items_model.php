<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Items_model extends CI_Model
{
    
    private $myDb = 'db_yess';
     private $myTable = 'mt_items';
     private $itemsId;
     private $colorId;
     private $sizeId;
     private $itemsCode;
     private $itemsName;
     private $material;
     private $segment;
     private $isNewArrival;
     private $isBestSeller;
     private $imgName;
     private $imgPath;
     private $weight;
     private $remarks;
     private $isActive;
     private $picInput;
     private $inputTime;
     private $picEdit;
     private $editTime;
     /* END PRIVATE VARIABLES */
     /* START CONSTRUCTOR */
     public function __construct()
     {
        parent::__construct();
          $this->itemsId = 0;
          $this->colorId = 0;
          $this->sizeId = 0;
          $this->itemsCode = '';
          $this->itemsName = '';
          $this->material = '';
          $this->segment = '';
          $this->isNewArrival = 0;
          $this->isBestSeller = 0;
          $this->imgName = '';
          $this->imgPath = '';
          $this->weight = 0;
          $this->remarks = '';
          $this->isActive = 0;
          $this->picInput = '';
          $this->inputTime = '0000-00-00 00:00:00';
          $this->picEdit = '';
          $this->editTime = '0000-00-00 00:00:00';
     }
     /* END CONSTRUCTOR */
     
     /* START GENERATE SETTER AND GETTER */
     public function setItemsId($aItemsId)
     {
        $this->itemsId = $this->db->escape_str($aItemsId);
     }
     public function getItemsId()
     {
        return $this->itemsId;
     }
     public function setColorId($aColorId)
     {
        $this->colorId = $this->db->escape_str($aColorId);
     }
     public function getColorId()
     {
        return $this->colorId;
     }
     public function setSizeId($aSizeId)
     {
        $this->sizeId = $this->db->escape_str($aSizeId);
     }
     public function getSizeId()
     {
        return $this->sizeId;
     }
     public function setItemsCode($aItemsCode)
     {
        $this->itemsCode = $this->db->escape_str($aItemsCode);
     }
     public function getItemsCode()
     {
        return $this->itemsCode;
     }
     public function setItemsName($aItemsName)
     {
        $this->itemsName = $this->db->escape_str($aItemsName);
     }
     public function getItemsName()
     {
        return $this->itemsName;
     }
     public function setMaterial($aMaterial)
     {
        $this->material = $this->db->escape_str($aMaterial);
     }
     public function getMaterial()
     {
        return $this->material;
     }
     public function setSegment($aSegment)
     {
        $this->segment = $this->db->escape_str($aSegment);
     }
     public function getSegment()
     {
        return $this->segment;
     }
     public function setIsNewArrival($aIsNewArrival)
     {
        $this->isNewArrival = $this->db->escape_str($aIsNewArrival);
     }
     public function getIsNewArrival()
     {
        return $this->isNewArrival;
     }
     public function setIsBestSeller($aIsBestSeller)
     {
        $this->isBestSeller = $this->db->escape_str($aIsBestSeller);
     }
     public function getIsBestSeller()
     {
        return $this->isBestSeller;
     }
     public function setImgName($aImgName)
     {
        $this->imgName = $this->db->escape_str($aImgName);
     }
     public function getImgName()
     {
        return $this->imgName;
     }
     public function setImgPath($aImgPath)
     {
        $this->imgPath = $this->db->escape_str($aImgPath);
     }
     public function getImgPath()
     {
        return $this->imgPath;
     }
     public function setWeight($aWeight)
     {
        $this->weight = $this->db->escape_str($aWeight);
     }
     public function getWeight()
     {
        return $this->weight;
     }
     public function setRemarks($aRemarks)
     {
        $this->remarks = $this->db->escape_str($aRemarks);
     }
     public function getRemarks()
     {
        return $this->remarks;
     }
     public function setIsActive($aIsActive)
     {
        $this->isActive = $this->db->escape_str($aIsActive);
     }
     public function getIsActive()
     {
        return $this->isActive;
     }
     public function setPicInput($aPicInput)
     {
        $this->picInput = $this->db->escape_str($aPicInput);
     }
     public function getPicInput()
     {
        return $this->picInput;
     }
     public function setInputTime($aInputTime)
     {
        $this->inputTime = $this->db->escape_str($aInputTime);
     }
     public function getInputTime()
     {
        return $this->inputTime;
     }
     public function setPicEdit($aPicEdit)
     {
        $this->picEdit = $this->db->escape_str($aPicEdit);
     }
     public function getPicEdit()
     {
        return $this->picEdit;
     }
     public function setEditTime($aEditTime)
     {
        $this->editTime = $this->db->escape_str($aEditTime);
     }
     public function getEditTime()
     {
        return $this->editTime;
     }
     /* END GENERATE SETTER AND GETTER */
     /* START INSERT */
     public function insert()
     {
        if($this->itemsId =='' || $this->itemsId == NULL )
        {
            $this->itemsId = 0;
        }
        if($this->colorId =='' || $this->colorId == NULL )
        {
            $this->colorId = 0;
        }
        if($this->sizeId =='' || $this->sizeId == NULL )
        {
            $this->sizeId = 0;
        }
        if($this->itemsCode =='' || $this->itemsCode == NULL )
        {
            $this->itemsCode = '';
        }
        if($this->itemsName =='' || $this->itemsName == NULL )
        {
            $this->itemsName = '';
        }
        if($this->material =='' || $this->material == NULL )
        {
            $this->material = '';
        }
        if($this->segment =='' || $this->segment == NULL )
        {
            $this->segment = '';
        }
        if($this->isNewArrival =='' || $this->isNewArrival == NULL )
        {
            $this->isNewArrival = 0;
        }
        if($this->isBestSeller =='' || $this->isBestSeller == NULL )
        {
            $this->isBestSeller = 0;
        }
        if($this->imgName =='' || $this->imgName == NULL )
        {
            $this->imgName = '';
        }
        if($this->imgPath =='' || $this->imgPath == NULL )
        {
            $this->imgPath = '';
        }
        if($this->weight =='' || $this->weight == NULL )
        {
            $this->weight = 0;
        }
        if($this->remarks =='' || $this->remarks == NULL )
        {
            $this->remarks = '';
        }
        if($this->isActive =='' || $this->isActive == NULL )
        {
            $this->isActive = 0;
        }
        if($this->picInput =='' || $this->picInput == NULL )
        {
            $this->picInput = '';
        }
        if($this->inputTime =='' || $this->inputTime == NULL )
        {
            $this->inputTime = '0000-00-00 00:00:00';
        }
        if($this->picEdit =='' || $this->picEdit == NULL )
        {
            $this->picEdit = '';
        }
        if($this->editTime =='' || $this->editTime == NULL )
        {
            $this->editTime = '0000-00-00 00:00:00';
        }
        
        $stQuery  = 'INSERT INTO '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= '( '; 
        $stQuery .=   'items_id,'; 
        $stQuery .=   'color_id,'; 
        $stQuery .=   'size_id,'; 
        $stQuery .=   'items_code,'; 
        $stQuery .=   'items_name,'; 
        $stQuery .=   'material,'; 
        // $stQuery .=   'segment,'; 
        $stQuery .=   'is_new_arrival,'; 
        $stQuery .=   'is_best_seller,'; 
        $stQuery .=   'img_name,'; 
        // $stQuery .=   'img_path,'; 
        $stQuery .=   'weight,'; 
        $stQuery .=   'remarks,'; 
        $stQuery .=   'is_active,'; 
        $stQuery .=   'pic_input,'; 
        $stQuery .=   'input_time'; 
        // $stQuery .=   'pic_edit,'; 
        // $stQuery .=   'edit_time'; 
        $stQuery .= ') '; 
        $stQuery .= 'VALUES '; 
        $stQuery .= '( '; 
        $stQuery .=   $this->db->escape_str($this->itemsId).','; 
        $stQuery .=   $this->db->escape_str($this->colorId).','; 
        $stQuery .=   $this->db->escape_str($this->sizeId).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->itemsCode).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->itemsName).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->material).'",'; 
        // $stQuery .=   '"'.$this->db->escape_str($this->segment).'",'; 
        $stQuery .=   $this->db->escape_str($this->isNewArrival).','; 
        $stQuery .=   $this->db->escape_str($this->isBestSeller).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->imgName).'",'; 
        // $stQuery .=   '"'.$this->db->escape_str($this->imgPath).'",'; 
        $stQuery .=   $this->db->escape_str($this->weight).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->remarks).'",'; 
        $stQuery .=   $this->db->escape_str($this->isActive).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->picInput).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->inputTime).'"'; 
        // $stQuery .=   '"'.$this->db->escape_str($this->picEdit).'",'; 
        // $stQuery .=   '"'.$this->db->escape_str($this->editTime).'"'; 
        $stQuery .= '); '; 
        $this->db->query($stQuery); 
     }
     /* END INSERT */
     /* START UPDATE */
     public function update($id)
     {
        if($this->itemsId =='' || $this->itemsId == NULL )
        {
            $this->itemsId = 0;
        }
        if($this->colorId =='' || $this->colorId == NULL )
        {
            $this->colorId = 0;
        }
        if($this->sizeId =='' || $this->sizeId == NULL )
        {
            $this->sizeId = 0;
        }
        if($this->itemsCode =='' || $this->itemsCode == NULL )
        {
            $this->itemsCode = '';
        }
        if($this->itemsName =='' || $this->itemsName == NULL )
        {
            $this->itemsName = '';
        }
        if($this->material =='' || $this->material == NULL )
        {
            $this->material = '';
        }
        if($this->segment =='' || $this->segment == NULL )
        {
            $this->segment = '';
        }
        if($this->isNewArrival =='' || $this->isNewArrival == NULL )
        {
            $this->isNewArrival = 0;
        }
        if($this->isBestSeller =='' || $this->isBestSeller == NULL )
        {
            $this->isBestSeller = 0;
        }
        if($this->imgName =='' || $this->imgName == NULL )
        {
            $this->imgName = '';
        }
        if($this->imgPath =='' || $this->imgPath == NULL )
        {
            $this->imgPath = '';
        }
        if($this->weight =='' || $this->weight == NULL )
        {
            $this->weight = 0;
        }
        if($this->remarks =='' || $this->remarks == NULL )
        {
            $this->remarks = '';
        }
        if($this->isActive =='' || $this->isActive == NULL )
        {
            $this->isActive = 0;
        }
        if($this->picInput =='' || $this->picInput == NULL )
        {
            $this->picInput = '';
        }
        if($this->inputTime =='' || $this->inputTime == NULL )
        {
            $this->inputTime = '0000-00-00 00:00:00';
        }
        if($this->picEdit =='' || $this->picEdit == NULL )
        {
            $this->picEdit = '';
        }
        if($this->editTime =='' || $this->editTime == NULL )
        {
            $this->editTime = '0000-00-00 00:00:00';
        }
        
        $stQuery  = 'UPDATE '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= 'SET '; 
        // $stQuery .=   'items_id ='.$this->db->escape_str($this->itemsId).','; 
        $stQuery .=   'color_id ='.$this->db->escape_str($this->colorId).','; 
        $stQuery .=   'size_id ='.$this->db->escape_str($this->sizeId).','; 
        $stQuery .=   'items_code ="'.$this->db->escape_str($this->itemsCode).'",'; 
        $stQuery .=   'items_name ="'.$this->db->escape_str($this->itemsName).'",'; 
        $stQuery .=   'material ="'.$this->db->escape_str($this->material).'",'; 
        // $stQuery .=   'segment ="'.$this->db->escape_str($this->segment).'",'; 
        $stQuery .=   'is_new_arrival ='.$this->db->escape_str($this->isNewArrival).','; 
        $stQuery .=   'is_best_seller ='.$this->db->escape_str($this->isBestSeller).','; 
        $stQuery .=   'img_name ="'.$this->db->escape_str($this->imgName).'",'; 
        // $stQuery .=   'img_path ="'.$this->db->escape_str($this->imgPath).'",'; 
        $stQuery .=   'weight ='.$this->db->escape_str($this->weight).','; 
        $stQuery .=   'remarks ="'.$this->db->escape_str($this->remarks).'",'; 
        // $stQuery .=   'is_active ='.$this->db->escape_str($this->isActive).','; 
        // $stQuery .=   'pic_input ="'.$this->db->escape_str($this->picInput).'",'; 
        // $stQuery .=   'input_time ="'.$this->db->escape_str($this->inputTime).'",'; 
        $stQuery .=   'pic_edit ="'.$this->db->escape_str($this->picEdit).'",'; 
        $stQuery .=   'edit_time ="'.$this->db->escape_str($this->editTime).'" '; 
        $stQuery .= 'WHERE '; 
        $stQuery .=   'items_id = '.$this->db->escape_str($id).''; 
        // test($stQuery,0);
        $this->db->query($stQuery); 
     }
     /* END UPDATE */
     /* START DELETE */
    
    function get_items()
    {
        $sql  = ' SELECT a.*,b.color_name,c.size_code,c.size_title,c.weight,d.price,d.disc_price FROM mt_items a ';
        $sql .= ' LEFT JOIN mt_color b ON a.color_id=b.color_id ';
        $sql .= ' LEFT JOIN mt_size c ON a.size_id=c.size_id ';
        $sql .= ' LEFT JOIN mt_price d ON a.items_id=d.items_id ';
        $sql .= ' WHERE a.is_active=1 ORDER BY a.items_id DESC ';
        $query = $this->db->query($sql);
        return $query->result();
    }

    function max_id(){
        $sql        = 'SELECT IFNULL(MAX(items_id),0)+1 id FROM mt_items';
        return $this->db->query($sql)->row();
    }

    function detail_items($id){
        $query = $this->db->query("SELECT a.*,b.* FROM mt_items a LEFT JOIN mt_price b ON b.items_id=a.items_id WHERE a.items_id='".$id."'")->row();
        return $query;
    }

    function act_delete_js(){
        $sql = "UPDATE mt_items SET is_active='0', pic_edit = '".$this->current_user['user_id']."', edit_time = '".dbnow()."' WHERE items_id = '".$this->input->post('id')."'";
        //test($sql,1);
        $query = $this->db->query($sql);
        return $query;
    }

    function f_newitems(){
        return $this->db->query("SELECT a.items_id,a.color_id,a.size_id,a.items_code,a.items_name,a.material,a.is_new_arrival,a.is_best_seller,a.img_name,b.price_code,b.price,
                                    b.disc_price,c.current_stock
                                    FROM mt_items a
                                    JOIN mt_price b ON b.items_id=a.items_id
                                    left JOIN tr_stock c ON c.items_id=a.items_id
                                    WHERE a.is_active='1' ORDER BY a.items_id DESC LIMIT 30");
    }

    function f_items_category($category){
        return $this->db->query("SELECT a.items_id,a.color_id,a.size_id,a.items_code,a.items_name,a.material,a.is_new_arrival,a.is_best_seller,a.img_name,b.price_code,b.price,
                                    b.disc_price,c.current_stock
                                    FROM mt_items a
                                    JOIN mt_price b ON b.items_id=a.items_id
                                    LEFT JOIN tr_stock c ON c.items_id=a.items_id
                                    LEFT JOIN br_items_category d ON a.items_id=d.items_id
                                    WHERE a.is_active='1' AND d.category_id='".$category."' ORDER BY a.items_id DESC");
    }

    function get_items_list($limit, $start){
        $query      = $this->db->query("SELECT a.items_id,a.color_id,a.size_id,a.items_code,a.items_name,a.material,a.is_new_arrival,a.is_best_seller,a.img_name,b.price_code,
                                    b.price,b.disc_price,c.current_stock
                                    FROM mt_items a
                                    JOIN mt_price b ON b.items_id=a.items_id
                                    left JOIN tr_stock c ON c.items_id=a.items_id
                                    WHERE a.is_active='1' ORDER BY a.items_id DESC LIMIT ".$limit.", ".$start." ");
        return $query;
    }

    function f_items($keterangan=false){
        $query  = "SELECT a.items_id,a.color_id,a.size_id,a.items_code,a.items_name,a.material,a.is_new_arrival,a.is_best_seller,
                    a.img_name,b.price_code,b.price,b.disc_price,c.current_stock
                    FROM mt_items a
                    JOIN mt_price b ON b.items_id=a.items_id
                    left JOIN tr_stock c ON c.items_id=a.items_id
                    WHERE a.is_active='1' ";
        if($keterangan=='best'){
        $query .= " AND a.is_best_seller='1' ";    
        }else if($keterangan=='new'){
        $query .= " AND a.is_new_arrival='1' ";   
        }
        $query .= " ORDER BY a.items_id DESC";

        return $this->db->query($query);
    }


}	