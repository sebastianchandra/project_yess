<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Items_color extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Master', 'active_submenu' => 'master/items_color'));  
        $this->load->model('master/items_color_model');
    }

	function index(){
        $data['data_list']          = $this->items_color_model->get_color();
        $this->template->load('body', 'master/items_color/items_color_view',$data);
	}

    function form(){
        $this->template->load('body', 'master/items_color/items_color_form');
    }

    function form_act(){
        $max_id             = $this->items_color_model->max_id()->id;
        $this->items_color_model->setColorId($this->security->xss_clean($max_id));
        $this->items_color_model->setColorCode($this->security->xss_clean($_POST['kode']));
        $this->items_color_model->setColorName($this->security->xss_clean($_POST['nama']));
        $save   = $this->items_color_model->insert();
        jsout(array('success' => true, 'status' => $save ));
    }

    function delete_js(){
        $delete = $this->items_color_model->act_delete_js();
        //test($delete,1);
        jsout(array('success' => true, 'status' => $delete ));
    }

    function edit($id){
        $data['detail']       = $this->items_color_model->detail_color($id);
        $this->template->load('body', 'master/items_color/items_color_edit', $data);
    }

    function edit_act(){
        $id = $this->security->xss_clean($_POST['id']);
        $this->M_mt_color->getObjectById($id);
        $this->M_mt_color->setColorCode($this->security->xss_clean($_POST['kode']));
        $this->M_mt_color->setColorName($this->security->xss_clean($_POST['nama']));
        $update     = $this->M_mt_color->update($id);

        jsout(array('success' => true, 'status' => $update ));
    }

}
?>