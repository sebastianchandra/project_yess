<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Items extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Master', 'active_submenu' => 'master/items'));  
        $this->load->model('master/items_model');
    }

	function index(){
        $data['data_items']     = $this->items_model->get_items();
        $this->template->load('body', 'master/items/items_view',$data);
	}

    function form(){
        $this->load->model('master/items_color_model');
        $this->load->model('master/items_size_model');
        $this->load->model('master/items_category_model');
        $data['data_size']          = $this->items_size_model->get_size();         
        $data['data_color']         = $this->items_color_model->get_color();         
        $data['data_category']      = $this->items_category_model->get_category();         
        $this->template->load('body', 'master/items/items_form',$data);
    }

    function add_file($pr_no){
        // test($pr_no,1);
        $config['upload_path'] = './assets_web/images/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['file_name'] = $pr_no;

        $this->load->library('upload', $config);
        $this->upload->overwrite = true;

        if (!$this->upload->do_upload('file_quotation')) {
            $error = $this->upload->display_errors();
            // test($error,1);
        } else {
            $result = $this->upload->data();
            // test($result,1);
        }   
    }

    function form_act(){
        $this->load->model('master/detail_category_model');
        $this->load->model('master/items_price_model');        

        $max_id             = $this->items_model->max_id()->id;        
        $new                = 0;
        $best               = 0;

        if($_POST['new']=='true'){
            $new            = 1;
        }else{
            $new            = 0;
        }

        if($_POST['best']=='true'){
            $best           = 1;
        }else{
            $best           = 0;            
        }

        $this->items_model->setItemsId($this->security->xss_clean($max_id));
        $this->items_model->setColorId($this->security->xss_clean($_POST['warna']));
        $this->items_model->setSizeId($this->security->xss_clean($_POST['ukuran']));
        $this->items_model->setItemsCode($this->security->xss_clean($_POST['kode']));
        $this->items_model->setItemsName($this->security->xss_clean($_POST['nama']));
        $this->items_model->setMaterial($this->security->xss_clean($_POST['material']));
        // $this->items_model->setSegment($this->security->xss_clean($_POST['segment']));
        $this->items_model->setIsNewArrival($this->security->xss_clean($new));
        $this->items_model->setIsBestSeller($this->security->xss_clean($best));
        $this->items_model->setImgName($this->security->xss_clean($_POST['file_upload']));
        // $this->items_model->setImgPath($this->security->xss_clean($_POST['imgPath']));
        $this->items_model->setWeight($this->security->xss_clean($_POST['berat']));
        $this->items_model->setRemarks($this->security->xss_clean($_POST['summernote']));
        $this->items_model->setIsActive($this->security->xss_clean(1));
        $this->items_model->setPicInput($this->security->xss_clean($this->current_user['user_id']));
        $this->items_model->setInputTime($this->security->xss_clean(dbnow()));

        $save   = $this->items_model->insert();

        $kategori           = $this->input->post('kategori');
        $count              = count($kategori);

        for ($i=0; $i < $count; $i++) { 
            $this->detail_category_model->setItemsId($this->security->xss_clean($max_id));
            $this->detail_category_model->setCategoryId($this->security->xss_clean($kategori[$i]));
            $this->detail_category_model->insert();
        }

        $max_id_price       = $this->items_price_model->max_id()->id;

        $this->items_price_model->setPriceId($this->security->xss_clean($max_id_price));
        $this->items_price_model->setItemsId($this->security->xss_clean($max_id));
        $this->items_price_model->setPriceCode($this->security->xss_clean($_POST['kode_harga']));
        $this->items_price_model->setPrice($this->security->xss_clean($_POST['harga']));
        $this->items_price_model->setDiscPrice($this->security->xss_clean($_POST['disc']));
        $this->items_price_model->setPicInput($this->security->xss_clean($this->current_user['user_id']));
        $this->items_price_model->setInputTime($this->security->xss_clean(dbnow()));
        $this->items_price_model->insert();

        jsout(array('success' => true, 'status' => $save, 'nama_file' => $max_id));
    }

    function delete($id){
        $delete = $this->items_model->act_delete($id);
        //test($delete,1);
        if($delete === true){
            $data['message'] = 'message';
            redirect('master/items',$data);
        }
    }

    function delete_js(){
        $delete = $this->items_model->act_delete_js();
        //test($delete,1);
        jsout(array('success' => true, 'status' => $delete ));
    }

    function edit($id){
        $kategori_barang        = array();
        $this->load->model('master/items_color_model');
        $this->load->model('master/items_size_model');
        $this->load->model('master/items_category_model');
        $data['data_size']          = $this->items_size_model->get_size();         
        $data['data_color']         = $this->items_color_model->get_color();         
        $data['data_category']      = $this->items_category_model->get_category();   

        $data['detail']             = $this->items_model->detail_items($id);
        $data['category']           = $this->detail_kategori($id);
        $this->template->load('body', 'master/items/items_edit',$data);
    }

    function detail_kategori($id){
        $brg            = array();
        $this->load->model('master/items_category_model');
        $kategori                   = $this->items_category_model->get_category_detail($id);

        foreach ($kategori as $key => $value) {
            $brg[] = $value->category_id;
        }
        return json_encode($brg);

    }

    function edit_act(){
        $this->load->model('master/detail_category_model');
        $this->load->model('master/items_price_model');  

        $id = $this->security->xss_clean($_POST['id']);

        if($_POST['new']=='true'){
            $new            = 1;
        }else{
            $new            = 0;
        }

        if($_POST['best']=='true'){
            $best           = 1;
        }else{
            $best           = 0;            
        }

        // $this->items_model->getObjectById($id);
        // $this->items_model->setItemsId($this->security->xss_clean($_POST['itemsId']));
        $this->items_model->setColorId($this->security->xss_clean($_POST['warna']));
        $this->items_model->setSizeId($this->security->xss_clean($_POST['ukuran']));
        $this->items_model->setItemsCode($this->security->xss_clean($_POST['kode']));
        $this->items_model->setItemsName($this->security->xss_clean($_POST['nama']));
        $this->items_model->setMaterial($this->security->xss_clean($_POST['material']));
        // $this->items_model->setSegment($this->security->xss_clean($_POST['segment']));
        $this->items_model->setIsNewArrival($this->security->xss_clean($new));
        $this->items_model->setIsBestSeller($this->security->xss_clean($best));
        if($_POST['file_upload']!=''){
            $this->items_model->setImgName($this->security->xss_clean($_POST['file_upload']));
        }else{
            $this->items_model->setImgName($this->security->xss_clean($_POST['file_upload_old']));
        }
        // $this->items_model->setImgPath($this->security->xss_clean($_POST['imgPath']));
        $this->items_model->setWeight($this->security->xss_clean($_POST['berat']));
        $this->items_model->setRemarks($this->security->xss_clean($_POST['summernote']));
        $this->items_model->setPicEdit($this->security->xss_clean($this->current_user['user_id']));
        $this->items_model->setEditTime($this->security->xss_clean(dbnow()));
        
        $update   = $this->items_model->update($id);

        $kategori           = $this->input->post('kategori');
        $count              = count($kategori);

        $this->detail_category_model->delete($id);

        for ($i=0; $i < $count; $i++) {
            $this->detail_category_model->setItemsId($this->security->xss_clean($id));
            $this->detail_category_model->setCategoryId($this->security->xss_clean($kategori[$i]));
            $this->detail_category_model->insert();
        }

        $id_harga = $this->security->xss_clean($_POST['id_harga']);
        $this->items_price_model->setItemsId($this->security->xss_clean($id));
        $this->items_price_model->setPriceCode($this->security->xss_clean($_POST['kode_harga']));
        $this->items_price_model->setPrice($this->security->xss_clean($_POST['harga']));
        $this->items_price_model->setDiscPrice($this->security->xss_clean($_POST['disc']));
        $this->items_price_model->setPicEdit($this->security->xss_clean($this->current_user['user_id']));
        $this->items_price_model->setEditTime($this->security->xss_clean(dbnow()));
        $this->items_price_model->update($id_harga);

        jsout(array('success' => true, 'status' => $update ));
    }

}
?>