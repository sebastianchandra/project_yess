<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Items_size extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Master', 'active_submenu' => 'master/items_size'));  
        $this->load->model('master/items_size_model');
    }

	function index(){
        $data['data_list']          = $this->items_size_model->get_size();
        $this->template->load('body', 'master/items_size/items_size_view',$data);
	}

    function form(){
        $this->template->load('body', 'master/items_size/items_size_form');
    }

    function form_act(){
        $max_id             = $this->items_size_model->max_id()->id;
        $this->items_size_model->setSizeId($this->security->xss_clean($max_id));
        $this->items_size_model->setSizeCode($this->security->xss_clean($_POST['kode']));
        $this->items_size_model->setSizeTitle($this->security->xss_clean($_POST['judul_size']));
        // $this->items_size_model->setSizeContent($this->security->xss_clean($_POST['sizeContent']));
        $this->items_size_model->setWeight($this->security->xss_clean($_POST['berat']));
        $this->items_size_model->setPicInput($this->security->xss_clean($this->current_user['user_id']));
        $this->items_size_model->setInputTime($this->security->xss_clean(dbnow()));

        $save   = $this->items_size_model->insert();
        jsout(array('success' => true, 'status' => $save ));
    }

    function delete_js(){
        $delete = $this->items_size_model->act_delete_js();
        //test($delete,1);
        jsout(array('success' => true, 'status' => $delete ));
    }

    function edit($id){
        $data['detail']       = $this->items_size_model->detail_size($id);
        $this->template->load('body', 'master/items_size/items_size_edit', $data);
    }

    function edit_act(){
        $id = $this->security->xss_clean($_POST['size_id']);
        $this->items_size_model->setSizeCode($this->security->xss_clean($_POST['kode']));
        $this->items_size_model->setSizeTitle($this->security->xss_clean($_POST['judul_size']));
        // $this->items_size_model->setSizeContent($this->security->xss_clean($_POST['sizeContent']));
        $this->items_size_model->setWeight($this->security->xss_clean($_POST['berat']));
        $this->items_size_model->setPicEdit($this->security->xss_clean($this->current_user['user_id']));
        $this->items_size_model->setEditTime($this->security->xss_clean(dbnow()));
        $update   = $this->items_size_model->update($id);

        jsout(array('success' => true, 'status' => $update ));
    }

}
?>