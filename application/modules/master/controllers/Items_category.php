<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Items_category extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Master', 'active_submenu' => 'master/items_category'));  
        $this->load->model('master/items_category_model');
    }

	function index(){
        $data['data_list']          = $this->items_category_model->get_category();
        $this->template->load('body', 'master/items_category/items_category_view',$data);
	}

    function form(){
        $this->template->load('body', 'master/items_category/items_category_form');
    }

    function form_act(){
        $max_id             = $this->items_category_model->max_id()->id;
        $this->items_category_model->setCategoryId($this->security->xss_clean($max_id));
        $this->items_category_model->setCategoryCode($this->security->xss_clean($_POST['kd_kategori']));
        $this->items_category_model->setCategoryName($this->security->xss_clean($_POST['nm_kategori']));
        $this->items_category_model->setRemarks($this->security->xss_clean($_POST['info_kategori']));
        $this->items_category_model->setPicInput($this->security->xss_clean($this->current_user['user_id']));
        $this->items_category_model->setInputTime($this->security->xss_clean(dbnow()));

        $save   = $this->items_category_model->insert();
        jsout(array('success' => true, 'status' => $save ));
    }

    function delete_js(){
        $delete = $this->items_category_model->act_delete_js();
        //test($delete,1);
        jsout(array('success' => true, 'status' => $delete ));
    }

    function edit($id){
        $data['detail']       = $this->items_category_model->detail_category($id);
        $this->template->load('body', 'master/items_category/items_category_edit', $data);
    }

    function edit_act(){
        $id = $this->security->xss_clean($_POST['id_kategori']);
        $this->items_category_model->setCategoryCode($this->security->xss_clean($_POST['kd_kategori']));
        $this->items_category_model->setCategoryName($this->security->xss_clean($_POST['nm_kategori']));
        $this->items_category_model->setRemarks($this->security->xss_clean($_POST['info_kategori']));
        $this->items_category_model->setPicEdit($this->security->xss_clean($this->current_user['user_id']));
        $this->items_category_model->setEditTime($this->security->xss_clean(dbnow()));
        $update   = $this->items_category_model->update($id);

        jsout(array('success' => true, 'status' => $update ));
    }

}
?>