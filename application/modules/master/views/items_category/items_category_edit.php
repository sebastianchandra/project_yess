<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Edit Kategori Barang</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Kategori Barang</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <!-- <a href="<?php echo base_url('master/items/form'); ?>" class="btn btn-primary">Input</a> -->
        </div>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Kode Kategori</p>
                            </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control" id="kd_kategori" value="<?= $detail->category_code; ?>">
                                <input type="hidden" class="form-control" id="id_kategori" value="<?= $detail->category_id; ?>">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Nama Kategori</p>
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control" id="nm_kategori" value="<?= $detail->category_name; ?>">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Info</p>
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="info_kategori" placeholder="Kategori Info" value="<?= $detail->remarks; ?>">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <button type="button" class="btn btn-primary btn-sm" id="save">Simpan</button>
                                <a class="btn btn-warning btn-sm" href="<?php echo base_url('master/items'); ?>">Batal</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$('#kd_kategori').focus();

$(document).ready(function(){
    $('#save').click(
        function(e){
        e.preventDefault();
        if(!$('#kd_kategori').val()){
            toastr.error("<b>Kode Kategori</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#items_name').select2('open');
            return false;
        }

        if($('#nm_kategori').val().length>=151){
            toastr.error("<b>Items Name</b> Max 150 Huruf", 'Alert', {"positionClass": "toast-top-center"});
            $("#items_name").focus();
            return false;
        }

        $.ajax({
            url  : baseUrl+'master/items_category/edit_act',
            type : "POST",  
            data : {
                id_kategori          : $('#id_kategori').val(),
                kd_kategori          : $('#kd_kategori').val(),
                nm_kategori          : $('#nm_kategori').val(),
                info_kategori        : $('#info_kategori').val(),
            },
            success : function(resp){
                if(resp.status == 'ERROR INSERT' || resp.status==false){
                    toastr.error("Data Gagal disimpan.", 'Alert', {"positionClass": "toast-top-center"});
                    return false;

                }else{
                    toastr.success("Data Berhasil di Simpan.", 'Alert', {"positionClass": "toast-top-center"});

                    setTimeout(function(){
                        window.location.href = baseUrl+'master/items_category/'; //will redirect to google.
                    }, 2000);
                }
            }
        });
    });
});
</script>