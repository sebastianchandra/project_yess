<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Input Barang</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Barang</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <!-- <a href="<?php echo base_url('master/items/form'); ?>" class="btn btn-primary">Input</a> -->
        </div>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Kode Barang</p>
                            </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control" id="kode">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Nama Barang</p>
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="nama" placeholder="Nama Barang">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Kategori</p>
                            </div>
                            <div class="col-md-8">
                                <select class="form-control" multiple="multiple" id="kategori">
                                    <?php 
                                    foreach ($data_category as $key => $value) {
                                        echo "<option value='".$value->category_id."'>".$value->category_name."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <!-- <div class="row-form">
                            <div class="col-md-2">
                                <p>Berat</p>
                            </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control" id="berat">
                            </div>
                        </div> -->
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Material</p>
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="material">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>New Arrival</p>
                            </div>
                            <div class="col-md-1">
                                <input type="checkbox" value="1" id="new">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Best Seller</p>
                            </div>
                            <div class="col-md-1">
                                <input type="checkbox" value="1" id="best">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Color</p>
                            </div>
                            <div class="col-md-3">
                                <select class="form-control" id="warna">
                                    <option value="">- Pilih -</option>
                                    <?php 
                                    foreach ($data_color as $key => $value) {
                                        echo "<option value='".$value->color_id."''>".$value->color_name."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Size</p>
                            </div>
                            <div class="col-md-3">
                                <select class="form-control" id="ukuran">
                                    <option value="">- Pilih -</option>
                                    <?php 
                                    foreach ($data_size as $key => $value) {
                                        echo "<option value='".$value->size_id."' data-berat='".$value->weight."'>".$value->size_code."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Kode Price</p>
                            </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control" id="kode_harga">
                            </div>
                            <div class="col-md-1">
                                <p>Price</p>
                            </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control" id="harga">
                            </div>
                            <div class="col-md-1">
                                <p>Discount</p>
                            </div>
                            <div class="col-md-2" style="padding-right: 2px !important;">
                                <input type="text" class="form-control" id="disc">
                            </div>
                            <span class="col-md-1" style="padding-left: 1px !important;color: red;">*Nominal</span>                             
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Images</p>
                            </div>
                            <div class="col-md-5">
                                <div class="custom-file">
                                    <input id="file_upload" type="file" class="custom-file-input" accept=".jpg, .jpeg, .png">
                                    <label for="logo" class="custom-file-label">Choose file...</label>
                                </div> 
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Deskripsi</p>
                            </div>
                            <div class="col-md-8">
                                <div class="ibox-content no-padding">
                                    <textarea class="form-control" rows="4" placeholder="Enter your address" id="summernote"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <button type="button" class="btn btn-primary btn-sm" id="save">Simpan</button>
                                <a class="btn btn-warning btn-sm" href="<?php echo base_url('master/items'); ?>">Batal</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$('#kode').focus();
$("#kategori").select2();
$("#ukuran").select2();
$("#warna").select2();
$('#summernote').summernote();

$('.custom-file-input').on('change', function() {
   let fileName = $(this).val().split('\\').pop();
   $(this).next('.custom-file-label').addClass("selected").html(fileName);
}); 

function addfile(file){
    var fd = new FormData();
    fd.append('file_quotation',file.file_quotation);

    $.post({
        url: baseUrl+'master/items/add_file/'+file.nama_file,
        data: fd,
        cache: false,
        contentType: false,
        processData: false
    });
}

$(document).ready(function(){
    $('#save').click(
        function(e){           

        e.preventDefault();
        if(!$('#kode').val()){
            toastr.error("<b>Kode Size</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#kode').focus();
            return false;
        }

        if(!$('#nama').val()){
            toastr.error("<b>Nama</b> TIdak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $("#nama").focus();
            return false;
        }

        $.ajax({
            url  : baseUrl+'master/items/form_act',
            type : "POST",  
            data : {
                kode            : $('#kode').val(),
                nama            : $('#nama').val(),
                kategori        : $('#kategori').val(),
                material        : $('#material').val(),
                new             : $("#new").is(':checked'),
                best            : $("#best").is(':checked'),
                warna           : $('#warna').val(),
                ukuran          : $('#ukuran').val(),
                berat           : $('#ukuran option:selected').attr('data-berat'),
                kode_harga      : $('#kode_harga').val(),
                harga           : $('#harga').val(),
                disc            : $('#disc').val(),
                summernote      : $('#summernote').val(),
                file_upload     : $('#file_upload')[0].files[0].name
            },
            success : function(resp){

                let file_quotation = $('#file_upload')[0].files[0];

                file = {
                    file_quotation  : file_quotation,
                    tipe_file       : file_quotation.type,
                    nama_file       : file_quotation.name
                };

                addfile(file);

                if(resp.status == 'ERROR INSERT' || resp.status==false){
                    toastr.error("Data Gagal disimpan.", 'Alert', {"positionClass": "toast-top-center"});
                    return false;

                }else{
                    toastr.success("Data Berhasil di Simpan.", 'Alert', {"positionClass": "toast-top-center"});

                    setTimeout(function(){
                        window.location.href = baseUrl+'master/items/'; //will redirect to google.
                    }, 2000);
                }
            }
        });
    });
});
</script>