<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>View Warna barang</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo base_url(); ?>">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Warna barang</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="<?php echo base_url('master/items_color/form'); ?>" class="btn btn-primary">Input</a>
        </div>
    </div>
</div>
<?php 
// test($data_items,1);
?>
<div class="wrapper wrapper-content">
  <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox-content">
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th width="11%">Kode Warna</th>
                    <th>Nama Warna</th>
                    <th width="13%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  foreach ($data_list as $key => $value) {
                  ?>
                  <tr>
                    <td><?php echo $value->color_code; ?></td>
                    <td><?php echo $value->color_name; ?></td>
                    <td class="center">
                      <a href="<?php echo base_url('master/items_color/edit/'.$value->color_id); ?>" class="btn btn-danger btn-xs">Edit</a>
                      <button id="delete" data-id='<?php echo $value->color_id; ?>' class="btn btn-warning btn-xs" type="submit">Delete</button>
                    </td>
                  </tr>
                  <?php
                  }
                  ?>
                </tbody>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
  $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      {extend: 'excel', title: 'Master Barang', exportOptions:{messageTop: 'Master Barang', columns: [0,1,2,3,4,5]}},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#vtable').on('click','#delete', function(e){
    var id  = $(this).data('id');

    toastr.warning(
      'Apakah anda Ingin Menghapus ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-id="'+id+'">Ya</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">Tidak</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });
});
function deleteRow(e){
  var id  = $(e).data('id');

  $.ajax({
    data: {
      id  : id
    },
    type : "POST",
    url: baseUrl+'master/items_color/delete_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        toastr.success('Data Tidak berhasil di Hapus', 'Alert', {"positionClass": "toast-top-center"});
        return false;

      } else {
        toastr.success("Data has been Delete.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'master/items_category'; //will redirect to google.
        }, 2000);
      }
    }
  });
}

function updateHarga(e){
  $.ajax({
    type : "POST",
    url: baseUrl+'master/items/priceUpdate',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Hapus');
        return false;

      } else {
        toastr.success("Harga Berhasil di Update.", 'Alert', {"positionClass": "toast-top-center"});

        // setTimeout(function () {
        //   window.location.href = baseUrl+'master/items'; //will redirect to google.
        // }, 2000);
      }
    }
  });
}
</script>
