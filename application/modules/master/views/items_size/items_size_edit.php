<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Edit Size Barang</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Size Barang</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <!-- <a href="<?php echo base_url('master/items/form'); ?>" class="btn btn-primary">Input</a> -->
        </div>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Kode Size</p>
                            </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control" id="kode" value="<?= $detail->size_code; ?>">
                                <input type="hidden" class="form-control" id="size_id" value="<?= $detail->size_id; ?>">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Size Title</p>
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="judul_size" placeholder="Size Title" value="<?= $detail->size_title; ?>">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Berat</p>
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control" id="berat" value="<?= $detail->weight; ?>">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <button type="button" class="btn btn-primary btn-sm" id="save">Simpan</button>
                                <a class="btn btn-warning btn-sm" href="<?php echo base_url('master/items'); ?>">Batal</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$('#kode').focus();

$(document).ready(function(){
    $('#save').click(
        function(e){
        e.preventDefault();
        if(!$('#kode').val()){
            toastr.error("<b>Kode Size</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#kode').select2('open');
            return false;
        }

        if(!$('#judul_size').val()){
            toastr.error("<b>Size Title</b> TIdak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $("#judul_size").focus();
            return false;
        }

        $.ajax({
            url  : baseUrl+'master/items_size/edit_act',
            type : "POST",  
            data : {
                size_id         : $('#size_id').val(),
                kode            : $('#kode').val(),
                judul_size      : $('#judul_size').val(),
                berat           : $('#berat').val()
            },
            success : function(resp){
                if(resp.status == 'ERROR INSERT' || resp.status==false){
                    toastr.error("Data Gagal disimpan.", 'Alert', {"positionClass": "toast-top-center"});
                    return false;

                }else{
                    toastr.success("Data Berhasil di Simpan.", 'Alert', {"positionClass": "toast-top-center"});

                    setTimeout(function(){
                        window.location.href = baseUrl+'master/items_size/'; //will redirect to google.
                    }, 2000);
                }
            }
        });
    });
});
</script>