<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function index($kategory=false){
		$this->session->set_userdata('ses_menu', array('active_menu' => 'product', 'sub_menu' => $kategory)); 
		$this->load->model('master/items_category_model');
		$this->load->model('master/items_model');
		$data['dkategori']		= $this->items_category_model->get_category();
		if($kategory==''){
			$data['dbarang']		= $this->items_model->f_items()->result();
		}else{
			$data['dbarang']		= $this->items_model->f_items_category($kategory)->result();
		}
		$this->template->load('body_home','front/f_product',$data);
	}

	function best(){
		$this->session->set_userdata('ses_menu', array('active_menu' => 'best', 'sub_menu' => '')); 
		$this->load->model('master/items_category_model');
		$this->load->model('master/items_model');
		$data['dkategori']		= $this->items_category_model->get_category();
		$data['dbarang']		= $this->items_model->f_items('best')->result();
		$this->template->load('body_home','front/f_product',$data);
	}

	function new(){
		$this->session->set_userdata('ses_menu', array('active_menu' => 'new', 'sub_menu' => '')); 
		$this->load->model('master/items_category_model');
		$this->load->model('master/items_model');
		$data['dkategori']		= $this->items_category_model->get_category();
		$data['dbarang']		= $this->items_model->f_items('new')->result();
		$this->template->load('body_home','front/f_product',$data);
	}

	function category($category=false)
	{
		$this->session->set_userdata('ses_menu', array('active_menu' => $category)); 

		$this->load->model('master/items_category_model');
		$this->load->model('front/items_model');

		$data['list_category'] 	= $this->items_category_model->get_category($category);
		$data['list_barang']	= $this->items_model->get_items($category);
		// test($data['list_barang']['total_rows'],1);
		$this->template->load('body_home','front/home/view_category',$data);
	}

	function detail($id){
		$this->load->model('master/items_category_model');
		$this->load->model('master/items_model');
		$data['list_category'] = $this->items_category_model->get_category();
		$data['detail_barang'] = $this->items_model->detail_items($id);
		$this->template->load('body_home','front/home/view_detail',$data);
	}



}
