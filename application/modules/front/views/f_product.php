<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col">
				<p class="bread"><span><a href="index.html">Home</a></span> / <span>Product</span></p>
			</div>
		</div>
	</div>
</div>

<div class="colorlib-product">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-xl-3">
				<div class="row">
					<div class="col-sm-12">
						<div class="sideProduct border mb-1">
							<div class="<?= ($this->session->userdata('ses_menu')['active_menu']=='new')? 'activeButton' : 'block-26'; ?>  mb-1">
			               		<ul>
									<li>
										<a href="<?= base_url('new'); ?>"><strong>New Arrival</strong></a>
									</li>
								</ul>
							</div>
						</div>
						<div class="sideProduct border mbest-1">
							<div class="<?= ($this->session->userdata('ses_menu')['active_menu']=='best')? 'activeButton' : 'block-26'; ?> mb-1">
			               		<ul>
									<li>
										<a href="<?= base_url('best'); ?>"><strong>Best Seller</strong></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="sideProduct border mb-1">
							<h3 style="padding: 1px 10px 1px 5px">Product</h3>
							<div class="block-26 mb-1">
			               		<ul>
			               			<?php
			               			foreach ($dkategori as $key => $value) {
			               				echo ($value->category_id==$this->session->userdata('ses_menu')['sub_menu']) ? '<li class="submenu">' : '<li>';
			               				echo '<a href="'.base_url('product/'.$value->category_id).'">'.$value->category_name.'</a>';
			               				echo "</li>";
			               			}
			               			?>
			               		</ul>
			            	</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-9 col-xl-9">
				<div class="row row-pb-md">
					<?php 
					foreach ($dbarang as $key => $value) {
						if($value->disc_price > 0){
							$disc 		= ($value->disc_price / $value->price)*100;
						}else{
							$disc 		= 0;
						}

						$potongan 	= $value->price - $value->disc_price;
					?>	
						<div class="col-lg-3 mb-4 text-center">
							<div class="product-entry border">
								<a href="#" class="prod-img"><img src="<?php echo base_url() ?>/assets_web/images/<?= $value->img_name; ?>" class="img-fluid" alt="Free html5 bootstrap 4 assets_home"></a>
								<div class="desc cRataTengah">
									<div class="cNamaBarang">
										<a href="#"><?= $value->items_name; ?></a>
									</div>
									<table width="100%" class="cGarisAtas">
										<tr>
											<td width="65%" align="left"><span class="cPriceDisc"><?= ($value->disc_price>0)? 'Rp. '.number_format($value->price) : ''; ?></span></td>
											<td width="35%" align="right"><small class="cDisc"><?= ($value->disc_price>0)? 'Disc '.number_format($disc,2).'%' : '&nbsp'; ?></small></td>
										</tr>
										<tr>
											<td colspan="2" align="center"><span class="price">Rp. <?= number_format($potongan); ?></span></td>
										</tr>
									</table>								
									<table width="100%" class="cGarisAtas">
										<tr>
											<td width="50%" align="left"><small><?= $value->items_code ?></small></td>
											<td width="50%" align="right">
												<?= ($value->current_stock>0)? '<small class="btn-info">Stok Tersedia</small>' : '<small class="btn-danger">Stok Habis</small>'; ?>
											</td>
										</tr>
									</table>								
								</div>
							</div>
						</div>
					<?php
					}
					?>
 				<div class="w-100"></div>
				<div class="row">
					<div class="col-md-12 text-center">
						<div class="block-27">
			                <ul>
				                <li><a href="#"><i class="ion-ios-arrow-back"></i></a></li>
								<li class="active"><span>1</span></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
								<li><a href="#"><i class="ion-ios-arrow-forward"></i></a></li>
			                </ul>
		            	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>