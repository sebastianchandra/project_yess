<?php 
// test($items,1);
?>
<aside id="colorlib-hero">
	<div class="flexslider">
		<ul class="slides">
	   	<li style="background-image: url(<?php echo base_url() ?>/assets_home/images/banner/download1.jpg);">
	   		<div class="overlay"></div>
	   		<div class="container-fluid">
	   			<div class="row">
		   			<div class="col-sm-6 offset-sm-3 text-center slider-text">
		   				<div class="slider-text-inner">
		   					<div class="desc">
			   					<!-- <h1 class="head-1">Men's</h1>
			   					<h2 class="head-2">Shoes</h2>
			   					<h2 class="head-3">Collection</h2>
			   					<p class="category"><span>New trending shoes</span></p>
			   					<p><a href="#" class="btn btn-primary">Shop Collection</a></p> -->
		   					</div>
		   				</div>
		   			</div>
		   		</div>
	   		</div>
	   	</li>
	   	<li style="background-image: url(<?php echo base_url() ?>/assets_home/images/banner/download4.jpg);">
	   		<div class="overlay"></div>
	   		<div class="container-fluid">
	   			<div class="row">
		   			<div class="col-sm-6 offset-sm-3 text-center slider-text">
		   				<div class="slider-text-inner">
		   					<div class="desc">
			   					<!-- <h1 class="head-1">Huge</h1>
			   					<h2 class="head-2">Sale</h2>
			   					<h2 class="head-3"><strong class="font-weight-bold">50%</strong> Off</h2>
			   					<p class="category"><span>Big sale sandals</span></p>
			   					<p><a href="#" class="btn btn-primary">Shop Collection</a></p> -->
		   					</div>
		   				</div>
		   			</div>
		   		</div>
	   		</div>
	   	</li>
	   	<li style="background-image: url(<?php echo base_url() ?>/assets_home/images/banner/download3.jpg);">
	   		<div class="overlay"></div>
	   		<div class="container-fluid">
	   			<div class="row">
		   			<div class="col-sm-6 offset-sm-3 text-center slider-text">
		   				<div class="slider-text-inner">
		   					<div class="desc">
			   					<!-- <h1 class="head-1">New</h1>
			   					<h2 class="head-2">Arrival</h2>
			   					<h2 class="head-3">up to <strong class="font-weight-bold">30%</strong> off</h2>
			   					<p class="category"><span>New stylish shoes for men</span></p>
			   					<p><a href="#" class="btn btn-primary">Shop Collection</a></p> -->
		   					</div>
		   				</div>
		   			</div>
		   		</div>
	   		</div>
	   	</li>
	  	</ul>
  	</div>
</aside>

<div class="colorlib-product">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 offset-sm-2 text-center colorlib-heading">
				<h2>Product Terbaru</h2>
			</div>
		</div>
		<div class="row row-pb-md">
			<?php 
			$numOfCols = 4;
			$rowCount = 0;
			$bootstrapColWidth = 12 / $numOfCols;
			
			foreach ($items as $key => $value) {
				if($value->disc_price > 0){
					$disc 		= ($value->disc_price / $value->price)*100;
				}else{
					$disc 		= 0;
				}

				$potongan 	= $value->price - $value->disc_price;
			?>
			<div class="col-lg-2 mb-4">
				<div class="product-entry border">
					<a href="#" class="prod-img">
						<img src="<?php echo base_url() ?>/assets_web/images/<?= $value->img_name; ?>" class="img-fluid" alt="Free html5 bootstrap 4 assets_home">
					</a>
					<div class="desc cRataTengah">
						<div class="cNamaBarang">
							<a href="#"><?= $value->items_name; ?></a>
						</div>
						<table width="100%" class="cGarisAtas">
							<tr>
								<td width="65%" align="left"><span class="cPriceDisc"><?= ($value->disc_price>0)? 'Rp. '.number_format($value->price) : ''; ?></span></td>
								<td width="35%" align="right"><small class="cDisc"><?= ($value->disc_price>0)? 'Disc '.number_format($disc,2).'%' : '&nbsp'; ?></small></td>
							</tr>
							<tr>
								<td colspan="2" align="center"><span class="price">Rp. <?= number_format($potongan); ?></span></td>
							</tr>
						</table>								
						<table width="100%" class="cGarisAtas">
							<tr>
								<td width="50%" align="left"><small><?= $value->items_code ?></small></td>
								<td width="50%" align="right">
									<?= ($value->current_stock>0)? '<small class="btn-info">Stok Tersedia</small>' : '<small class="btn-danger">Stok Habis</small>' ?>
								</td>
							</tr>
						</table>								
					</div>
				</div>
			</div>
			<?php 
			}
			?>
			<div class="w-100"></div>

		</div>

		<!-- <div class="row">
			<div class="col-md-12 text-center">
				<p><a href="#" class="btn btn-primary btn-lg">Shop All Products</a></p>
			</div>
		</div> -->
	</div>
</div>