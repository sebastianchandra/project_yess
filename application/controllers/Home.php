<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	// public function index()
	// {
	// 	$this->load->library('pagination');
	// 	$this->load->model('master/items_model');

	// 	$config['base_url'] = base_url(); //site url
 //        $config['total_rows'] = $this->items_model->f_newitems()->num_rows(); //total row
 //        $config['per_page'] = 1;  //show record per halaman
 //        $config["uri_segment"] = 2;  // uri parameter
 //        $choice = $config["total_rows"] / $config["per_page"];
 //        $config["num_links"] = floor($choice);

 //        $config['first_link']       = 'First';
 //        $config['last_link']        = 'Last';
 //        $config['next_link']        = 'Next';
 //        $config['prev_link']        = 'Prev';
 //        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
 //        $config['full_tag_close']   = '</ul></nav></div>';
 //        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
 //        $config['num_tag_close']    = '</span></li>';
 //        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
 //        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
 //        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
 //        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
 //        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
 //        $config['prev_tagl_close']  = '</span>Next</li>';
 //        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
 //        $config['first_tagl_close'] = '</span></li>';
 //        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
 //        $config['last_tagl_close']  = '</span></li>';

 //        $this->pagination->initialize($config);
 //        $data['page'] = ($this->uri->segment(2)) ? $this->uri->segment(2) : 1;

 //        $data['items'] = $this->items_model->get_items_list($config["per_page"], $data['page']); 
 //        $data['pagination'] = $this->pagination->create_links();
 //        // test($this->pagination->create_links(),1);
	// 	// $data['items']				= $this->items_model->f_newitems();
	// 	$this->template->load('body_home','front/f_terbaru',$data);
	// }

	public function index()
	{
		$this->load->model('master/items_model');
		$this->session->set_userdata('ses_menu', array('active_menu' => 'home')); 

		$data['items']				= $this->items_model->f_newitems()->result();
		$this->template->load('body_home','front/f_terbaru',$data);
	}

	function delivery(){
		$this->load->model('master/items_category_model');
		$data['list_category'] = $this->items_category_model->get_category();
		$this->template->load('body_home','front/home/view_delivery',$data);
	}

	function contact(){
		$this->load->model('master/items_category_model');
		$data['list_category'] = $this->items_category_model->get_category();
		$this->template->load('body_home','front/home/view_contact',$data);
	}

}
