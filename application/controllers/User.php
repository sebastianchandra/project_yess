<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('master/users_model');
        $this->load->library('form_validation');
    }


	function index(){
        // $data['company']    = $this->company_model->get_company();
		if ($this->current_user['loginuser'] == 0){
			$username 		= $this->security->xss_clean($this->db->escape_str($this->input->post('username')));
			$password		= $this->security->xss_clean($this->db->escape_str($this->input->post('password')));

			$this->form_validation->set_rules('username','Username');
            $this->form_validation->set_rules('password','Password');

			if ($username == '' ){
                $this->load->view('login');
            }else{
            	$usr_result = $this->users_model->get_user($username,$password);
                $row        = $this->users_model->detail_user($username,$password);

                if ($usr_result > 0){
                  	$session_data = array('user_id'        => $row->user_id,
                                            'nip'          => $row->nip,
                                            'full_name'    => $row->full_name,
                                            'user_level'   => $row->user_level,
                                            'loginuser'    => TRUE
                                        );
                  	//$this->session->sess_expiration = '60'; // 1 menit
                    $this->session->sess_expiration_on_close = 'true';
                    $this->session->set_userdata('ses_login', $session_data);

                    // $this->users_model->aktifitas_user('logged in');
                    redirect('welcome');

                }else{
                	$this->session->set_flashdata('msg','<div class="alert alert-danger text-center"><font size="2">Username Atau Password Anda salah</font></div>');
                    redirect($_SERVER['HTTP_REFERER']);
                    redirect('user');
                }
            } 
		}else{
            redirect('welcome');
		}
	}

	function logout() {
         //remove all session data
        if ($this->current_user['loginuser'] == 1){
            // $this->users_model->aktifitas_user('logged out');
        }

        $this->load->view('login');
    }
}
