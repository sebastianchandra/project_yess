<?php
if($this->current_user['loginuser']==1){
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Yess Motor</title>
    <link href="<?php echo base_url('assets_home/img/icon.png'); ?>" rel="SHORTCUT ICON">
    <link href="<?php echo base_url(); ?>assets_web/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets_web/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets_web/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets_web/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets_web/css/spinners.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>assets_web/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets_web/css/datepicker3.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets_web/css/daterangepicker-bs3.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets_web/css/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets_web/css/datatables.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets_web/css/toastr.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets_web/css/summernote-bs4.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>assets_web/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css">

    <script src="<?php echo base_url(); ?>assets_web/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets_web/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets_web/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>assets_web/js/jquery.metisMenu.js"></script>
    <script src="<?php echo base_url(); ?>assets_web/js/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url(); ?>assets_web/js/inspinia.js"></script>
    <script src="<?php echo base_url(); ?>assets_web/js/pace.min.js"></script>
    <script src="<?php echo base_url(); ?>assets_web/js/select2.full.min.js"></script>
    <script src="<?php echo base_url(); ?>assets_web/js/daterangepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets_web/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets_web/js/jasny-bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets_web/js/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets_web/js/dataTables.bootstrap4.min.js"></script>
    <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/datatables/1.9.4/jquery.dataTables.js" data-semver="1.9.4" data-require="datatables@*"></script> -->
    <script src="<?php echo base_url(); ?>assets_web/js/toastr.min.js"></script>
    <script src="<?php echo base_url(); ?>assets_web/js/bootstrap-dialog.min.js"></script>
    <script src="<?php echo base_url(); ?>assets_web/js/summernote-bs4.js"></script>

    <script>
        var baseUrl = '<?php echo base_url(); ?>';

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
    </script>

    <style type="text/css">
        @media print {
            .no-print {
              visibility: hidden !important;
            }
            .do-print {
              visibility: visible !important;
            }
        }
        
        .dataTable tbody td.col_left {
            text-align: left;
        }
        .dataTable tbody td.col_center {
            text-align: center;
        }
        .dataTable tbody td.col_right {
            text-align: right;
        }

    </style>
</head>
<?php 
?>
<body class="">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">

                    <li class="nav-header">
                        <div class="dropdown profile-element" style="text-align: center;font-size: 18px;font-weight: 600;color: white;padding: 18px 0;">
                            <div>Yess Motor</div>
                        </div>
                        <div class="logo-element">SiP</div>
                    </li>
                    <li <?php echo $this->session->userdata('ses_menu')['active_menu'] == 'Dashboard' ? 'class="active"' : '' ?>>
                        <a href="<?= base_url('welcome'); ?>"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span></a>
                    </li>        
                    <li <?php echo $this->session->userdata('ses_menu')['active_menu'] == 'Master' ? 'class="active"' : '' ?> >
                        <a href="#">
                            <i class="fa fa-edit"></i> <span class="nav-label">Master</span><span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse">
                            <li <?= $this->session->userdata('ses_menu')['active_submenu'] == 'master/items' ? 'class="active"' : '' ?>><a href="<?= base_url('master/items') ?>">Barang</a></li>
                        </ul>
                        <ul class="nav nav-second-level collapse">
                            <li <?= $this->session->userdata('ses_menu')['active_submenu'] == 'master/items_category' ? 'class="active"' : '' ?>><a href="<?= base_url('master/items_category') ?>">Kategori Barang</a></li>
                        </ul>
                        <ul class="nav nav-second-level collapse">
                            <li <?= $this->session->userdata('ses_menu')['active_submenu'] == 'master/items_size' ? 'class="active"' : '' ?>><a href="<?= base_url('master/items_size') ?>">Size Barang</a></li>
                        </ul>
                        <ul class="nav nav-second-level collapse">
                            <li <?= $this->session->userdata('ses_menu')['active_submenu'] == 'master/items_color' ? 'class="active"' : '' ?>><a href="<?= base_url('master/items_color') ?>">Warna Barang</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom no-print">
                <nav class="navbar navbar-static-top  " role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                        <form role="search" class="navbar-form-custom" action="search_results.html">
                            <div class="form-group">
                                <input type="text" disabled="" placeholder="V.1.2" class="form-control" name="top-search" id="top-search">
                            </div>
                        </form>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <a href="<?php echo base_url('logout'); ?>">
                                <i class="fa fa-sign-out"></i> Log out
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <?php 
                echo $contents; 
            ?>

            <script>

            function tanggal_indonesia(dateString)
            {
                var thisDate = dateString.split('-');
                var newDate = [thisDate[2],thisDate[1],thisDate[0] ].join("-");
                return newDate;
            }

            $(".price").on("keydown", function(e) {
                var keycode = (event.which) ? event.which : event.keyCode;
                if (e.shiftKey == true || e.ctrlKey == true) return false;
                if ([8, 110, 39, 37, 46, 9].indexOf(keycode) >= 0 || // allow tab, dot, left and right arrows, delete keys
                    (keycode == 190 && this.value.indexOf('.') === -1) || // allow dot if not exists in the value
                    (keycode == 110 && this.value.indexOf('.') === -1) || // allow dot if not exists in the value
                    (keycode >= 48 && keycode <= 57) || // allow numbers
                    (keycode >= 96 && keycode <= 105)) { // allow numpad numbers
                    // check for the decimals after dot and prevent any digits
                    var parts = this.value.split('.');
                if (parts.length > 1 && // has decimals
                    parts[1].length >= 2 && // should limit this
                    (
                        (keycode >= 48 && keycode <= 57) || (keycode >= 96 && keycode <= 105)
                    ) // requested key is a digit
                ){
                    return false;
                }else {
                    if (keycode == 110) {
                        this.value += ".";
                        return false;
                    }
                    return true;
                }
                } else {
                    return false;
                }
            }).on("keyup", function() {
                var parts = this.value.split('.');
                parts[0] = parts[0].replace(/,/g, '').replace(/^0+/g, '');
                if (parts[0] == "") parts[0] = "0";
                var calculated = parts[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                if (parts.length >= 2) calculated += "." + parts[1].substring(0, 2);
                this.value = calculated;
                if (this.value == "NaN" || this.value == "") this.value = 0;
            });
            
            </script>

            <div class="footer d-print-none">
                <div class="float-right">
                  <!-- 10GB of <strong>250GB</strong> Free. -->
                </div>
                <div>
                  <strong>Copyright</strong> Yess Motor
                </div>
            </div>
        </div>
    </div>

    <!-- Mainly scripts -->
    
</body>
</html>

<?php 
}else{
$this->session->set_flashdata('msg','<div class="alert alert-danger text-center"><font size="2">Harap Login Kembali.</font></div>');
redirect('login');
}
?>