<!DOCTYPE HTML>
<html>
	<head>
	<title>Toko Yess Motor</title>
   	<meta charset="utf-8">
   	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Rokkitt:100,300,400,700" rel="stylesheet">
	<link href="<?php echo base_url('assets_home/img/icon.png'); ?>" rel="SHORTCUT ICON">
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets_home/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets_home/css/icomoon.css">
	<!-- Ion Icon Fonts-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets_home/css/ionicons.min.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets_home/css/bootstrap.min.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets_home/css/magnific-popup.css">

	<!-- Flexslider  -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets_home/css/flexslider.css">

	<!-- Owl Carousel -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets_home/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets_home/css/owl.theme.default.min.css">
	
	<!-- Date Picker -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets_home/css/bootstrap-datepicker.css">
	<!-- Flaticons  -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets_home/fonts/flaticon/font/flaticon.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets_home/css/style.css">

	<script src="<?php echo base_url(); ?>assets_web/js/jquery-3.1.1.min.js"></script>

	<script>
        var baseUrl = '<?php echo base_url(); ?>';

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
    </script>
    
	</head>
	<body>
		
	<div class="colorlib-loader"></div>

	<div id="page">
		<nav class="colorlib-nav" role="navigation">
			<div class="top-menu">
				<div class="container">
					<div class="row">
						<div class="col-sm-7 col-md-9">
							<div id="colorlib-logo">
								<!-- <img src="<?php echo base_url('assets_home/img/logored.png'); ?>"> --> YESS MOTOR
							</div>
						</div>
						<div class="col-sm-5 col-md-3">
			            <form action="#" class="search-wrap">
			               <div class="form-group">
			                  <input type="search" class="form-control search" placeholder="Search">
			                  <button class="btn btn-primary submit-search text-center" type="submit"><i class="icon-search"></i></button>
			               </div>
			            </form>
			         </div>
		         </div>
					<div class="row">
						<div class="col-sm-12 text-left menu-1">
							<ul>
								<li <?= ($this->session->userdata('ses_menu')['active_menu']=='home')? 'class="active"' : ''; ?>><a href="<?= base_url(); ?>">Home</a></li>
								<!-- <li class="has-dropdown">
									<a href="men.html">Men</a>
									<ul class="dropdown">
										<li><a href="product-detail.html">Product Detail</a></li>
										<li><a href="cart.html">Shopping Cart</a></li>
										<li><a href="checkout.html">Checkout</a></li>
										<li><a href="order-complete.html">Order Complete</a></li>
										<li><a href="add-to-wishlist.html">Wishlist</a></li>
									</ul>
								</li> -->
								<li <?= ($this->session->userdata('ses_menu')['active_menu']=='new')? 'class="active"' : ''; ?>><a href="<?= base_url('new'); ?>">New Arrival</a></li>
								<li <?= ($this->session->userdata('ses_menu')['active_menu']=='best')? 'class="active"' : ''; ?>><a href="<?= base_url('best'); ?>">Best Seller</a></li>
								<li <?= ($this->session->userdata('ses_menu')['active_menu']=='product')? 'class="active"' : ''; ?>><a href="<?= base_url('product'); ?>">Product</a></li>
								<li><a href="contact.html">About Me</a></li>
								<li><a href="contact.html">Video</a></li>
								<li><a href="contact.html">Question</a></li>
								<li><a href="contact.html">Contact</a></li>
								<!-- <li class="cart"><a href="cart.html"><i class="icon-shopping-cart"></i> Cart [0]</a></li> -->
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="sale">
				<div class="container">
					<div class="row">
						<div class="col-sm-8 offset-sm-2 text-center">
							<div class="row"></div>
						</div>
					</div>
				</div>
			</div>
		</nav>
		

		<?php 
            echo $contents; 
        ?>

		<div class="colorlib-partner">
			<div class="container_partner">
				<div class="row">
					<div class="col-sm-8 offset-sm-2 text-center colorlib-heading colorlib-heading-sm">
						<h2>Trusted Partners</h2>
					</div>
				</div>
				<div class="row">
					<div class="col partner-col text-center">
						<img src="<?php echo base_url() ?>/assets_home/images/partner/ktm.png" class="img-partner" alt="Free html4 bootstrap 4 assets_home">
					</div>
					<div class="col partner-col text-center">
						<img src="<?php echo base_url() ?>/assets_home/images/partner/akrapovic.png" class="img-partner" alt="Free html4 bootstrap 4 assets_home">
					</div>
					<div class="col partner-col text-center">
						<img src="<?php echo base_url() ?>/assets_home/images/partner/motorex.png" class="img-partner" alt="Free html4 bootstrap 4 assets_home">
					</div>
					<div class="col partner-col text-center">
						<img src="<?php echo base_url() ?>/assets_home/images/partner/sidi.png" class="img-partner" alt="Free html4 bootstrap 4 assets_home">
					</div>
					<div class="col partner-col text-center">
						<img src="<?php echo base_url() ?>/assets_home/images/partner/acerbis.png" class="img-partner" alt="Free html4 bootstrap 4 assets_home">
					</div>
				</div>
			</div>
		</div>

		<footer id="colorlib-footer" role="contentinfo">
			<div class="container">
				<div class="row row-pb-md">
					<div class="col footer-col colorlib-widget">
						<h4>About Yess Motor</h4>
						<p></p>
						<p>
							<ul class="colorlib-social-icons">
								<li><a href="#"><i class="icon-twitter"></i></a></li>
								<li><a href="#"><i class="icon-facebook"></i></a></li>
								<li><a href="#"><i class="icon-linkedin"></i></a></li>
								<li><a href="#"><i class="icon-dribbble"></i></a></li>
							</ul>
						</p>
					</div>
					<div class="col footer-col colorlib-widget">
						<h4>Customer Service</h4>
						<p>
							<ul class="colorlib-footer-links">
								<li><a href="#">Contact</a></li>
								<li><a href="#">Delivery Information</a></li>
								<li><a href="#">Order Tracking</a></li>
								<li><a href="#">Returns Policy</a></li>
								<li><a href="#">How To Order</a></li>
								<li><a href="#">FAQ</a></li>
							</ul>
						</p>
					</div>
					<div class="col footer-col colorlib-widget">
						<h4>About Us</h4>
						<p>
							<ul class="colorlib-footer-links">
								<li><a href="#">Our Company</a></li>
								<li><a href="#">Privacy Policy</a></li>
								<li><a href="#">Terms Of Use</a></li>
							</ul>
						</p>
					</div>
					<div class="col footer-col">
						<h4>Contact Information</h4>
						<ul class="colorlib-footer-links">
							<li>291 South 21th Street, <br> Suite 721 New York NY 10016</li>
							<li><a href="tel://1234567920">+ 1235 2355 98</a></li>
							<li><a href="mailto:info@yoursite.com">info@yoursite.com</a></li>
							<li><a href="#">yoursite.com</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="copy">
				<div class="row">
					<div class="col-sm-12 text-center">
						<p>
							<span>
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This  is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://www.instagram.com/karbo.sys/" target="_blank">Karbo.sys</a></span> 
						</p>
					</div>
				</div>
			</div>
		</footer>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="ion-ios-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="<?php echo base_url(); ?>assets_home/js/jquery.min.js"></script>
   <!-- popper -->
   <script src="<?php echo base_url(); ?>assets_home/js/popper.min.js"></script>
   <!-- bootstrap 4.1 -->
   <script src="<?php echo base_url(); ?>assets_home/js/bootstrap.min.js"></script>
   <!-- jQuery easing -->
   <script src="<?php echo base_url(); ?>assets_home/js/jquery.easing.1.3.js"></script>
	<!-- Waypoints -->
	<script src="<?php echo base_url(); ?>assets_home/js/jquery.waypoints.min.js"></script>
	<!-- Flexslider -->
	<script src="<?php echo base_url(); ?>assets_home/js/jquery.flexslider-min.js"></script>
	<!-- Owl carousel -->
	<script src="<?php echo base_url(); ?>assets_home/js/owl.carousel.min.js"></script>
	<!-- Magnific Popup -->
	<script src="<?php echo base_url(); ?>assets_home/js/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo base_url(); ?>assets_home/js/magnific-popup-options.js"></script>
	<!-- Date Picker -->
	<script src="<?php echo base_url(); ?>assets_home/js/bootstrap-datepicker.js"></script>
	<!-- Stellar Parallax -->
	<script src="<?php echo base_url(); ?>assets_home/js/jquery.stellar.min.js"></script>
	<!-- Main -->
	<script src="<?php echo base_url(); ?>assets_home/js/main.js"></script>

	</body>
</html>

