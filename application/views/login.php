<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>Yess Motor | Login</title>

  <link href="<?php echo base_url(); ?>assets_web/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets_web/css/font-awesome.css" rel="stylesheet">

  <link href="<?php echo base_url(); ?>assets_web/css/animate.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets_web/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

  <div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
      <div>
        <h1 class="logo-name">Yess</h1>
      </div>
      <!-- <h3>Sistem Informasi Perkapalan</h3> -->
      <form class="m-t" role="form" action="<?php echo base_url('user'); ?>" autocomplete="off" method="post">
        <div class="form-group">
            <input type="text" name="username" class="form-control" placeholder="Username" required="">
        </div>
        <div class="form-group">
            <input type="password" name="password" class="form-control" placeholder="Password" required="">
        </div>
        <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
        <?php echo $this->session->flashdata('msg'); ?> 

        <!-- <a href="#"><small>Forgot password?</small></a>
        <p class="text-muted text-center"><small>Do not have an account?</small></p>
        <a class="btn btn-sm btn-white btn-block" href="register.html">Create an account</a> -->
      </form>
      <p class="m-t"> <small>Welcome Yess Motor</small> </p>
    </div>
  </div>

  <!-- Mainly scripts -->
  <script src="<?php echo base_url(); ?>assets_web/js/jquery-3.1.1.min.js"></script>
  <script src="<?php echo base_url(); ?>assets_web/js/popper.min.js"></script>
  <script src="<?php echo base_url(); ?>assets_web/js/bootstrap.js"></script>

</body>

</html>
