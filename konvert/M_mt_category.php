<?php
/*********************************************************************
 *  Created By       :  Generator Version 1.0.23                     *
 *  Created Date     :  Nov 19, 2020                                 *
 *  Description      : All code generated by model generator         *
 *  Generator Author : Tommy Maurice(tommy_maurice@yahoo.com)        *
 *********************************************************************/
defined('BASEPATH') OR exit('No direct script access allowed');
class M_mt_category extends CI_Model
{
     /* START PRIVATE VARIABLES */
     private $myDb = 'db_yess';
     private $myTable = 'mt_category';
     private $categoryId;
     private $categoryCode;
     private $categoryName;
     private $remarks;
     private $picInput;
     private $inputTime;
     private $picEdit;
     private $editTime;
     /* END PRIVATE VARIABLES */
     /* START CONSTRUCTOR */
     public function __construct()
     {
     	parent::__construct();
          $this->categoryId = 0;
          $this->categoryCode = '';
          $this->categoryName = '';
          $this->remarks = '';
          $this->picInput = '';
          $this->inputTime = '0000-00-00 00:00:00';
          $this->picEdit = '';
          $this->editTime = '0000-00-00 00:00:00';
     }
     /* END CONSTRUCTOR */
     
     /* START GENERATE SETTER AND GETTER */
     public function setCategoryId($aCategoryId)
     {
     	$this->categoryId = $this->db->escape_str($aCategoryId);
     }
     public function getCategoryId()
     {
     	return $this->categoryId;
     }
     public function setCategoryCode($aCategoryCode)
     {
     	$this->categoryCode = $this->db->escape_str($aCategoryCode);
     }
     public function getCategoryCode()
     {
     	return $this->categoryCode;
     }
     public function setCategoryName($aCategoryName)
     {
     	$this->categoryName = $this->db->escape_str($aCategoryName);
     }
     public function getCategoryName()
     {
     	return $this->categoryName;
     }
     public function setRemarks($aRemarks)
     {
     	$this->remarks = $this->db->escape_str($aRemarks);
     }
     public function getRemarks()
     {
     	return $this->remarks;
     }
     public function setPicInput($aPicInput)
     {
     	$this->picInput = $this->db->escape_str($aPicInput);
     }
     public function getPicInput()
     {
     	return $this->picInput;
     }
     public function setInputTime($aInputTime)
     {
     	$this->inputTime = $this->db->escape_str($aInputTime);
     }
     public function getInputTime()
     {
     	return $this->inputTime;
     }
     public function setPicEdit($aPicEdit)
     {
     	$this->picEdit = $this->db->escape_str($aPicEdit);
     }
     public function getPicEdit()
     {
     	return $this->picEdit;
     }
     public function setEditTime($aEditTime)
     {
     	$this->editTime = $this->db->escape_str($aEditTime);
     }
     public function getEditTime()
     {
     	return $this->editTime;
     }
     /* END GENERATE SETTER AND GETTER */
     /* START INSERT */
     public function insert()
     {
     	if($this->categoryId =='' || $this->categoryId == NULL )
     	{
          	$this->categoryId = 0;
     	}
     	if($this->categoryCode =='' || $this->categoryCode == NULL )
     	{
          	$this->categoryCode = '';
     	}
     	if($this->categoryName =='' || $this->categoryName == NULL )
     	{
          	$this->categoryName = '';
     	}
     	if($this->remarks =='' || $this->remarks == NULL )
     	{
          	$this->remarks = '';
     	}
     	if($this->picInput =='' || $this->picInput == NULL )
     	{
          	$this->picInput = '';
     	}
     	if($this->inputTime =='' || $this->inputTime == NULL )
     	{
          	$this->inputTime = '0000-00-00 00:00:00';
     	}
     	if($this->picEdit =='' || $this->picEdit == NULL )
     	{
          	$this->picEdit = '';
     	}
     	if($this->editTime =='' || $this->editTime == NULL )
     	{
          	$this->editTime = '0000-00-00 00:00:00';
     	}
     	
     	$stQuery  = 'INSERT INTO '.$this->myDb.'.'.$this->myTable.' '; 
     	$stQuery .= '( '; 
     	$stQuery .=   'category_id,'; 
     	$stQuery .=   'category_code,'; 
     	$stQuery .=   'category_name,'; 
     	$stQuery .=   'remarks,'; 
     	$stQuery .=   'pic_input,'; 
     	$stQuery .=   'input_time,'; 
     	$stQuery .=   'pic_edit,'; 
     	$stQuery .=   'edit_time'; 
     	$stQuery .= ') '; 
     	$stQuery .= 'VALUES '; 
     	$stQuery .= '( '; 
     	$stQuery .=   $this->db->escape_str($this->categoryId).','; 
     	$stQuery .=   '"'.$this->db->escape_str($this->categoryCode).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->categoryName).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->remarks).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->picInput).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->inputTime).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->picEdit).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->editTime).'"'; 
     	$stQuery .= '); '; 
     	$this->db->query($stQuery); 
     }
     /* END INSERT */
     /* START UPDATE */
     public function update($id)
     {
     	if($this->categoryId =='' || $this->categoryId == NULL )
     	{
          	$this->categoryId = 0;
     	}
     	if($this->categoryCode =='' || $this->categoryCode == NULL )
     	{
          	$this->categoryCode = '';
     	}
     	if($this->categoryName =='' || $this->categoryName == NULL )
     	{
          	$this->categoryName = '';
     	}
     	if($this->remarks =='' || $this->remarks == NULL )
     	{
          	$this->remarks = '';
     	}
     	if($this->picInput =='' || $this->picInput == NULL )
     	{
          	$this->picInput = '';
     	}
     	if($this->inputTime =='' || $this->inputTime == NULL )
     	{
          	$this->inputTime = '0000-00-00 00:00:00';
     	}
     	if($this->picEdit =='' || $this->picEdit == NULL )
     	{
          	$this->picEdit = '';
     	}
     	if($this->editTime =='' || $this->editTime == NULL )
     	{
          	$this->editTime = '0000-00-00 00:00:00';
     	}
     	
     	$stQuery  = 'UPDATE '.$this->myDb.'.'.$this->myTable.' '; 
     	$stQuery .= 'SET '; 
     	$stQuery .=   'category_id ='.$this->db->escape_str($this->categoryId).','; 
     	$stQuery .=   'category_code ="'.$this->db->escape_str($this->categoryCode).'",'; 
     	$stQuery .=   'category_name ="'.$this->db->escape_str($this->categoryName).'",'; 
     	$stQuery .=   'remarks ="'.$this->db->escape_str($this->remarks).'",'; 
     	$stQuery .=   'pic_input ="'.$this->db->escape_str($this->picInput).'",'; 
     	$stQuery .=   'input_time ="'.$this->db->escape_str($this->inputTime).'",'; 
     	$stQuery .=   'pic_edit ="'.$this->db->escape_str($this->picEdit).'",'; 
     	$stQuery .=   'edit_time ="'.$this->db->escape_str($this->editTime).'" '; 
     	$stQuery .= 'WHERE '; 
     	$stQuery .=   'category_id = '.$this->db->escape_str($id).''; 
     	$this->db->query($stQuery); 
     }
     /* END UPDATE */
     /* START DELETE */
     public function delete($id)
     {
     	$stQuery  = 'DELETE FROM '.$this->myDb.'.'.$this->myTable.' '; 
     	$stQuery .= 'WHERE category_id = '.$this->db->escape_str($id).''; 
     	$this->db->query($stQuery); 
     }
     /* END DELETE */
     /* START GET ALL DATA */
     public function getAll()
     {
     	$this->db->select('*');
     	$this->db->from($this->myDb.'.'.$this->myTable);
     	$this->db->order_by('category_id', 'ASC');
     	return $this->db->get()->result_array();
     }
     /* END GET ALL DATA */
     /* START GET DATA BY ID */
     public function getById($id)
     {
     	$this->db->select('*');
     	$this->db->from($this->myDb.'.'.$this->myTable);
     	$this->db->where('category_id', $this->db->escape_str($id));
     	return $this->db->get()->row_array();
     }
     /* END GET DATA BY ID */
     /* START GET OBJECT DATA BY ID */
     public function getObjectById($id)
     {
     	$this->db->select('*');
     	$this->db->from($this->myDb.'.'.$this->myTable);
     	$this->db->where('category_id', $this->db->escape_str($id));
     	$row = $this->db->get()->row_array();
     	$this->categoryId = $row['category_id']; 
     	$this->categoryCode = $row['category_code']; 
     	$this->categoryName = $row['category_name']; 
     	$this->remarks = $row['remarks']; 
     	$this->picInput = $row['pic_input']; 
     	$this->inputTime = $row['input_time']; 
     	$this->picEdit = $row['pic_edit']; 
     	$this->editTime = $row['edit_time']; 
     }
     /* END GET OBJECT DATA BY ID */
     /* START OF GET DATA COUNT */
     public function getDataCount()
     {
     	$stQuery  = 'SELECT category_id FROM '.$this->myDb.'.'.$this->myTable.' '; 
     	$query = $this->db->query($stQuery);
     	return $query->num_rows();
     }
     /* END OF GET DATA COUNT */
     /* START OF RESET VALUES */
     public function resetValues()
     {
     	$this->categoryId = 0; 
     	$this->categoryCode = ''; 
     	$this->categoryName = ''; 
     	$this->remarks = ''; 
     	$this->picInput = ''; 
     	$this->inputTime = '0000-00-00 00:00:00'; 
     	$this->picEdit = ''; 
     	$this->editTime = '0000-00-00 00:00:00'; 
     }
     /* END OF RESET VALUES */
}
?>
