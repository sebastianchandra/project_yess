<?php
/*********************************************************************
 *  Created By       :  Generator Version 1.0.23                     *
 *  Created Date     :  Nov 19, 2020                                 *
 *  Description      : All code generated by model generator         *
 *  Generator Author : Tommy Maurice(tommy_maurice@yahoo.com)        *
 *********************************************************************/
defined('BASEPATH') OR exit('No direct script access allowed');
class M_tr_outgoing_stock_01 extends CI_Model
{
     /* START PRIVATE VARIABLES */
     private $myDb = 'db_yess';
     private $myTable = 'tr_outgoing_stock_01';
     private $outId;
     private $outDate;
     private $outNo;
     private $outType;
     private $storeId;
     private $remarks;
     private $isCancel;
     private $picInput;
     private $inputTime;
     private $picEdit;
     private $editTime;
     /* END PRIVATE VARIABLES */
     /* START CONSTRUCTOR */
     public function __construct()
     {
     	parent::__construct();
          $this->outId = '';
          $this->outDate = "0000-00-00";
          $this->outNo = '';
          $this->outType = '';
          $this->storeId = '';
          $this->remarks = '';
          $this->isCancel = 0;
          $this->picInput = '';
          $this->inputTime = '0000-00-00 00:00:00';
          $this->picEdit = '';
          $this->editTime = '0000-00-00 00:00:00';
     }
     /* END CONSTRUCTOR */
     
     /* START GENERATE SETTER AND GETTER */
     public function setOutId($aOutId)
     {
     	$this->outId = $this->db->escape_str($aOutId);
     }
     public function getOutId()
     {
     	return $this->outId;
     }
     public function setOutDate($aOutDate)
     {
     	$this->outDate = $this->db->escape_str($aOutDate);
     }
     public function getOutDate()
     {
     	return $this->outDate;
     }
     public function setOutNo($aOutNo)
     {
     	$this->outNo = $this->db->escape_str($aOutNo);
     }
     public function getOutNo()
     {
     	return $this->outNo;
     }
     public function setOutType($aOutType)
     {
     	$this->outType = $this->db->escape_str($aOutType);
     }
     public function getOutType()
     {
     	return $this->outType;
     }
     public function setStoreId($aStoreId)
     {
     	$this->storeId = $this->db->escape_str($aStoreId);
     }
     public function getStoreId()
     {
     	return $this->storeId;
     }
     public function setRemarks($aRemarks)
     {
     	$this->remarks = $this->db->escape_str($aRemarks);
     }
     public function getRemarks()
     {
     	return $this->remarks;
     }
     public function setIsCancel($aIsCancel)
     {
     	$this->isCancel = $this->db->escape_str($aIsCancel);
     }
     public function getIsCancel()
     {
     	return $this->isCancel;
     }
     public function setPicInput($aPicInput)
     {
     	$this->picInput = $this->db->escape_str($aPicInput);
     }
     public function getPicInput()
     {
     	return $this->picInput;
     }
     public function setInputTime($aInputTime)
     {
     	$this->inputTime = $this->db->escape_str($aInputTime);
     }
     public function getInputTime()
     {
     	return $this->inputTime;
     }
     public function setPicEdit($aPicEdit)
     {
     	$this->picEdit = $this->db->escape_str($aPicEdit);
     }
     public function getPicEdit()
     {
     	return $this->picEdit;
     }
     public function setEditTime($aEditTime)
     {
     	$this->editTime = $this->db->escape_str($aEditTime);
     }
     public function getEditTime()
     {
     	return $this->editTime;
     }
     /* END GENERATE SETTER AND GETTER */
     /* START INSERT */
     public function insert()
     {
     	if($this->outId =='' || $this->outId == NULL )
     	{
          	$this->outId = '';
     	}
     	if($this->outDate =='' || $this->outDate == NULL )
     	{
          	$this->outDate = "0000-00-00";
     	}
     	if($this->outNo =='' || $this->outNo == NULL )
     	{
          	$this->outNo = '';
     	}
     	if($this->outType =='' || $this->outType == NULL )
     	{
          	$this->outType = '';
     	}
     	if($this->storeId =='' || $this->storeId == NULL )
     	{
          	$this->storeId = '';
     	}
     	if($this->remarks =='' || $this->remarks == NULL )
     	{
          	$this->remarks = '';
     	}
     	if($this->isCancel =='' || $this->isCancel == NULL )
     	{
          	$this->isCancel = 0;
     	}
     	if($this->picInput =='' || $this->picInput == NULL )
     	{
          	$this->picInput = '';
     	}
     	if($this->inputTime =='' || $this->inputTime == NULL )
     	{
          	$this->inputTime = '0000-00-00 00:00:00';
     	}
     	if($this->picEdit =='' || $this->picEdit == NULL )
     	{
          	$this->picEdit = '';
     	}
     	if($this->editTime =='' || $this->editTime == NULL )
     	{
          	$this->editTime = '0000-00-00 00:00:00';
     	}
     	
     	$stQuery  = 'INSERT INTO '.$this->myDb.'.'.$this->myTable.' '; 
     	$stQuery .= '( '; 
     	$stQuery .=   'out_id,'; 
     	$stQuery .=   'out_date,'; 
     	$stQuery .=   'out_no,'; 
     	$stQuery .=   'out_type,'; 
     	$stQuery .=   'store_id,'; 
     	$stQuery .=   'remarks,'; 
     	$stQuery .=   'is_cancel,'; 
     	$stQuery .=   'pic_input,'; 
     	$stQuery .=   'input_time,'; 
     	$stQuery .=   'pic_edit,'; 
     	$stQuery .=   'edit_time'; 
     	$stQuery .= ') '; 
     	$stQuery .= 'VALUES '; 
     	$stQuery .= '( '; 
     	$stQuery .=   '"'.$this->db->escape_str($this->outId).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->outDate).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->outNo).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->outType).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->storeId).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->remarks).'",'; 
     	$stQuery .=   $this->db->escape_str($this->isCancel).','; 
     	$stQuery .=   '"'.$this->db->escape_str($this->picInput).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->inputTime).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->picEdit).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->editTime).'"'; 
     	$stQuery .= '); '; 
     	$this->db->query($stQuery); 
     }
     /* END INSERT */
     /* START UPDATE */
     public function update($id)
     {
     	if($this->outId =='' || $this->outId == NULL )
     	{
          	$this->outId = '';
     	}
     	if($this->outDate =='' || $this->outDate == NULL )
     	{
          	$this->outDate = "0000-00-00";
     	}
     	if($this->outNo =='' || $this->outNo == NULL )
     	{
          	$this->outNo = '';
     	}
     	if($this->outType =='' || $this->outType == NULL )
     	{
          	$this->outType = '';
     	}
     	if($this->storeId =='' || $this->storeId == NULL )
     	{
          	$this->storeId = '';
     	}
     	if($this->remarks =='' || $this->remarks == NULL )
     	{
          	$this->remarks = '';
     	}
     	if($this->isCancel =='' || $this->isCancel == NULL )
     	{
          	$this->isCancel = 0;
     	}
     	if($this->picInput =='' || $this->picInput == NULL )
     	{
          	$this->picInput = '';
     	}
     	if($this->inputTime =='' || $this->inputTime == NULL )
     	{
          	$this->inputTime = '0000-00-00 00:00:00';
     	}
     	if($this->picEdit =='' || $this->picEdit == NULL )
     	{
          	$this->picEdit = '';
     	}
     	if($this->editTime =='' || $this->editTime == NULL )
     	{
          	$this->editTime = '0000-00-00 00:00:00';
     	}
     	
     	$stQuery  = 'UPDATE '.$this->myDb.'.'.$this->myTable.' '; 
     	$stQuery .= 'SET '; 
     	$stQuery .=   'out_id ="'.$this->db->escape_str($this->outId).'",'; 
     	$stQuery .=   'out_date ="'.$this->db->escape_str($this->outDate).'",'; 
     	$stQuery .=   'out_no ="'.$this->db->escape_str($this->outNo).'",'; 
     	$stQuery .=   'out_type ="'.$this->db->escape_str($this->outType).'",'; 
     	$stQuery .=   'store_id ="'.$this->db->escape_str($this->storeId).'",'; 
     	$stQuery .=   'remarks ="'.$this->db->escape_str($this->remarks).'",'; 
     	$stQuery .=   'is_cancel ='.$this->db->escape_str($this->isCancel).','; 
     	$stQuery .=   'pic_input ="'.$this->db->escape_str($this->picInput).'",'; 
     	$stQuery .=   'input_time ="'.$this->db->escape_str($this->inputTime).'",'; 
     	$stQuery .=   'pic_edit ="'.$this->db->escape_str($this->picEdit).'",'; 
     	$stQuery .=   'edit_time ="'.$this->db->escape_str($this->editTime).'" '; 
     	$stQuery .= 'WHERE '; 
     	$stQuery .=   'out_id = '.$this->db->escape_str('"'.$id.'"'); 
     	$this->db->query($stQuery); 
     }
     /* END UPDATE */
     /* START DELETE */
     public function delete($id)
     {
     	$stQuery  = 'DELETE FROM '.$this->myDb.'.'.$this->myTable.' '; 
     	$stQuery .= 'WHERE out_id = '.$this->db->escape_str($id).''; 
     	$this->db->query($stQuery); 
     }
     /* END DELETE */
     /* START GET ALL DATA */
     public function getAll()
     {
     	$this->db->select('*');
     	$this->db->from($this->myDb.'.'.$this->myTable);
     	$this->db->order_by('out_id', 'ASC');
     	return $this->db->get()->result_array();
     }
     /* END GET ALL DATA */
     /* START GET DATA BY ID */
     public function getById($id)
     {
     	$this->db->select('*');
     	$this->db->from($this->myDb.'.'.$this->myTable);
     	$this->db->where('out_id', $this->db->escape_str($id));
     	return $this->db->get()->row_array();
     }
     /* END GET DATA BY ID */
     /* START GET OBJECT DATA BY ID */
     public function getObjectById($id)
     {
     	$this->db->select('*');
     	$this->db->from($this->myDb.'.'.$this->myTable);
     	$this->db->where('out_id', $this->db->escape_str($id));
     	$row = $this->db->get()->row_array();
     	$this->outId = $row['out_id']; 
     	$this->outDate = $row['out_date']; 
     	$this->outNo = $row['out_no']; 
     	$this->outType = $row['out_type']; 
     	$this->storeId = $row['store_id']; 
     	$this->remarks = $row['remarks']; 
     	$this->isCancel = $row['is_cancel']; 
     	$this->picInput = $row['pic_input']; 
     	$this->inputTime = $row['input_time']; 
     	$this->picEdit = $row['pic_edit']; 
     	$this->editTime = $row['edit_time']; 
     }
     /* END GET OBJECT DATA BY ID */
     /* START OF GET DATA COUNT */
     public function getDataCount()
     {
     	$stQuery  = 'SELECT out_id FROM '.$this->myDb.'.'.$this->myTable.' '; 
     	$query = $this->db->query($stQuery);
     	return $query->num_rows();
     }
     /* END OF GET DATA COUNT */
     /* START OF RESET VALUES */
     public function resetValues()
     {
     	$this->outId = ''; 
     	$this->outDate = '0000-00-00'; 
     	$this->outNo = ''; 
     	$this->outType = ''; 
     	$this->storeId = ''; 
     	$this->remarks = ''; 
     	$this->isCancel = 0; 
     	$this->picInput = ''; 
     	$this->inputTime = '0000-00-00 00:00:00'; 
     	$this->picEdit = ''; 
     	$this->editTime = '0000-00-00 00:00:00'; 
     }
     /* END OF RESET VALUES */
}
?>
