<?php
/*********************************************************************
 *  Created By       : Generator Version 1.0.23                      *
 *  Created Date     : Nov 19, 2020                                 *
 *  Description      : All code generated by controller generator    *
 *  Generator Author : Tommy Maurice(tommy_maurice@yahoo.com)        *
 *********************************************************************/
/*********************************************************************
 * To use XSS Filter, Altering                                       *
 * your application/config/autoload.php file in the following way:   *
 * $autoload['helper']=array('security');                            *
 * Example : $data = $this->security->xss_clean($data);              *
 *********************************************************************/
defined('BASEPATH') OR exit('No direct script access allowed');
class Mt_color extends CI_Controller
{
     /* START CONSTRUCTOR */
     public function __construct()
     {
     	parent::__construct();
     	$this->load->helper('security');
     	$this->load->model('M_mt_color');
     }
     /* END CONSTRUCTOR */
     /* START INSERT */
     public function ins()
     {
     	$this->M_mt_color->setColorId($this->security->xss_clean($_POST['colorId']));
     	$this->M_mt_color->setColorCode($this->security->xss_clean($_POST['colorCode']));
     	$this->M_mt_color->setColorName($this->security->xss_clean($_POST['colorName']));
     	$this->M_mt_color->insert();
     }
     /* END INSERT */
     /* START UPDATE */
     public function upd()
     {
     	$id = $this->security->xss_clean($_POST['colorId']);
     	$this->M_mt_color->getObjectById($id);
     	$this->M_mt_color->setColorId($this->security->xss_clean($_POST['colorId']));
     	$this->M_mt_color->setColorCode($this->security->xss_clean($_POST['colorCode']));
     	$this->M_mt_color->setColorName($this->security->xss_clean($_POST['colorName']));
     	$this->M_mt_color->update($id);
     }
     /* END UPDATE */
     /* START DELETE */
     public function del()
     {
     	$id = $this->security->xss_clean($_POST['colorId']);
     	$this->M_mt_color->delete($id);
     }
     /* END DELETE */
     /* START GET ALL */
     public function loadAll()
     {
     	$rs = $this->M_mt_color->getAll();
     	echo json_encode($rs); 
     }
     /* END GET ALL */
}
?>
