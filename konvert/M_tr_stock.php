<?php
/*********************************************************************
 *  Created By       :  Generator Version 1.0.23                     *
 *  Created Date     :  Nov 19, 2020                                 *
 *  Description      : All code generated by model generator         *
 *  Generator Author : Tommy Maurice(tommy_maurice@yahoo.com)        *
 *********************************************************************/
defined('BASEPATH') OR exit('No direct script access allowed');
class M_tr_stock extends CI_Model
{
     /* START PRIVATE VARIABLES */
     private $myDb = 'db_yess';
     private $myTable = 'tr_stock';
     private $stockId;
     private $itemsId;
     private $docNo;
     private $storeId;
     private $trnDate;
     private $trnYear;
     private $trnMonth;
     private $activity;
     private $itemsIn;
     private $itemsOut;
     private $adjIn;
     private $adjOut;
     private $oldStock;
     private $currentStock;
     private $picData;
     private $dataTime;
     /* END PRIVATE VARIABLES */
     /* START CONSTRUCTOR */
     public function __construct()
     {
     	parent::__construct();
          $this->stockId = '';
          $this->itemsId = 0;
          $this->docNo = '';
          $this->storeId = '';
          $this->trnDate = "0000-00-00";
          $this->trnYear = '';
          $this->trnMonth = '';
          $this->activity = '';
          $this->itemsIn = 0;
          $this->itemsOut = 0;
          $this->adjIn = 0;
          $this->adjOut = 0;
          $this->oldStock = 0;
          $this->currentStock = 0;
          $this->picData = '';
          $this->dataTime = '0000-00-00 00:00:00';
     }
     /* END CONSTRUCTOR */
     
     /* START GENERATE SETTER AND GETTER */
     public function setStockId($aStockId)
     {
     	$this->stockId = $this->db->escape_str($aStockId);
     }
     public function getStockId()
     {
     	return $this->stockId;
     }
     public function setItemsId($aItemsId)
     {
     	$this->itemsId = $this->db->escape_str($aItemsId);
     }
     public function getItemsId()
     {
     	return $this->itemsId;
     }
     public function setDocNo($aDocNo)
     {
     	$this->docNo = $this->db->escape_str($aDocNo);
     }
     public function getDocNo()
     {
     	return $this->docNo;
     }
     public function setStoreId($aStoreId)
     {
     	$this->storeId = $this->db->escape_str($aStoreId);
     }
     public function getStoreId()
     {
     	return $this->storeId;
     }
     public function setTrnDate($aTrnDate)
     {
     	$this->trnDate = $this->db->escape_str($aTrnDate);
     }
     public function getTrnDate()
     {
     	return $this->trnDate;
     }
     public function setTrnYear($aTrnYear)
     {
     	$this->trnYear = $this->db->escape_str($aTrnYear);
     }
     public function getTrnYear()
     {
     	return $this->trnYear;
     }
     public function setTrnMonth($aTrnMonth)
     {
     	$this->trnMonth = $this->db->escape_str($aTrnMonth);
     }
     public function getTrnMonth()
     {
     	return $this->trnMonth;
     }
     public function setActivity($aActivity)
     {
     	$this->activity = $this->db->escape_str($aActivity);
     }
     public function getActivity()
     {
     	return $this->activity;
     }
     public function setItemsIn($aItemsIn)
     {
     	$this->itemsIn = $this->db->escape_str($aItemsIn);
     }
     public function getItemsIn()
     {
     	return $this->itemsIn;
     }
     public function setItemsOut($aItemsOut)
     {
     	$this->itemsOut = $this->db->escape_str($aItemsOut);
     }
     public function getItemsOut()
     {
     	return $this->itemsOut;
     }
     public function setAdjIn($aAdjIn)
     {
     	$this->adjIn = $this->db->escape_str($aAdjIn);
     }
     public function getAdjIn()
     {
     	return $this->adjIn;
     }
     public function setAdjOut($aAdjOut)
     {
     	$this->adjOut = $this->db->escape_str($aAdjOut);
     }
     public function getAdjOut()
     {
     	return $this->adjOut;
     }
     public function setOldStock($aOldStock)
     {
     	$this->oldStock = $this->db->escape_str($aOldStock);
     }
     public function getOldStock()
     {
     	return $this->oldStock;
     }
     public function setCurrentStock($aCurrentStock)
     {
     	$this->currentStock = $this->db->escape_str($aCurrentStock);
     }
     public function getCurrentStock()
     {
     	return $this->currentStock;
     }
     public function setPicData($aPicData)
     {
     	$this->picData = $this->db->escape_str($aPicData);
     }
     public function getPicData()
     {
     	return $this->picData;
     }
     public function setDataTime($aDataTime)
     {
     	$this->dataTime = $this->db->escape_str($aDataTime);
     }
     public function getDataTime()
     {
     	return $this->dataTime;
     }
     /* END GENERATE SETTER AND GETTER */
     /* START INSERT */
     public function insert()
     {
     	if($this->stockId =='' || $this->stockId == NULL )
     	{
          	$this->stockId = '';
     	}
     	if($this->itemsId =='' || $this->itemsId == NULL )
     	{
          	$this->itemsId = 0;
     	}
     	if($this->docNo =='' || $this->docNo == NULL )
     	{
          	$this->docNo = '';
     	}
     	if($this->storeId =='' || $this->storeId == NULL )
     	{
          	$this->storeId = '';
     	}
     	if($this->trnDate =='' || $this->trnDate == NULL )
     	{
          	$this->trnDate = "0000-00-00";
     	}
     	if($this->trnYear =='' || $this->trnYear == NULL )
     	{
          	$this->trnYear = '';
     	}
     	if($this->trnMonth =='' || $this->trnMonth == NULL )
     	{
          	$this->trnMonth = '';
     	}
     	if($this->activity =='' || $this->activity == NULL )
     	{
          	$this->activity = '';
     	}
     	if($this->itemsIn =='' || $this->itemsIn == NULL )
     	{
          	$this->itemsIn = 0;
     	}
     	if($this->itemsOut =='' || $this->itemsOut == NULL )
     	{
          	$this->itemsOut = 0;
     	}
     	if($this->adjIn =='' || $this->adjIn == NULL )
     	{
          	$this->adjIn = 0;
     	}
     	if($this->adjOut =='' || $this->adjOut == NULL )
     	{
          	$this->adjOut = 0;
     	}
     	if($this->oldStock =='' || $this->oldStock == NULL )
     	{
          	$this->oldStock = 0;
     	}
     	if($this->currentStock =='' || $this->currentStock == NULL )
     	{
          	$this->currentStock = 0;
     	}
     	if($this->picData =='' || $this->picData == NULL )
     	{
          	$this->picData = '';
     	}
     	if($this->dataTime =='' || $this->dataTime == NULL )
     	{
          	$this->dataTime = '0000-00-00 00:00:00';
     	}
     	
     	$stQuery  = 'INSERT INTO '.$this->myDb.'.'.$this->myTable.' '; 
     	$stQuery .= '( '; 
     	$stQuery .=   'stock_id,'; 
     	$stQuery .=   'items_id,'; 
     	$stQuery .=   'doc_no,'; 
     	$stQuery .=   'store_id,'; 
     	$stQuery .=   'trn_date,'; 
     	$stQuery .=   'trn_year,'; 
     	$stQuery .=   'trn_month,'; 
     	$stQuery .=   'activity,'; 
     	$stQuery .=   'items_in,'; 
     	$stQuery .=   'items_out,'; 
     	$stQuery .=   'adj_in,'; 
     	$stQuery .=   'adj_out,'; 
     	$stQuery .=   'old_stock,'; 
     	$stQuery .=   'current_stock,'; 
     	$stQuery .=   'pic_data,'; 
     	$stQuery .=   'data_time'; 
     	$stQuery .= ') '; 
     	$stQuery .= 'VALUES '; 
     	$stQuery .= '( '; 
     	$stQuery .=   '"'.$this->db->escape_str($this->stockId).'",'; 
     	$stQuery .=   $this->db->escape_str($this->itemsId).','; 
     	$stQuery .=   '"'.$this->db->escape_str($this->docNo).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->storeId).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->trnDate).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->trnYear).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->trnMonth).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->activity).'",'; 
     	$stQuery .=   $this->db->escape_str($this->itemsIn).','; 
     	$stQuery .=   $this->db->escape_str($this->itemsOut).','; 
     	$stQuery .=   $this->db->escape_str($this->adjIn).','; 
     	$stQuery .=   $this->db->escape_str($this->adjOut).','; 
     	$stQuery .=   $this->db->escape_str($this->oldStock).','; 
     	$stQuery .=   $this->db->escape_str($this->currentStock).','; 
     	$stQuery .=   '"'.$this->db->escape_str($this->picData).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->dataTime).'"'; 
     	$stQuery .= '); '; 
     	$this->db->query($stQuery); 
     }
     /* END INSERT */
     /* START UPDATE */
     public function update($id)
     {
     	if($this->stockId =='' || $this->stockId == NULL )
     	{
          	$this->stockId = '';
     	}
     	if($this->itemsId =='' || $this->itemsId == NULL )
     	{
          	$this->itemsId = 0;
     	}
     	if($this->docNo =='' || $this->docNo == NULL )
     	{
          	$this->docNo = '';
     	}
     	if($this->storeId =='' || $this->storeId == NULL )
     	{
          	$this->storeId = '';
     	}
     	if($this->trnDate =='' || $this->trnDate == NULL )
     	{
          	$this->trnDate = "0000-00-00";
     	}
     	if($this->trnYear =='' || $this->trnYear == NULL )
     	{
          	$this->trnYear = '';
     	}
     	if($this->trnMonth =='' || $this->trnMonth == NULL )
     	{
          	$this->trnMonth = '';
     	}
     	if($this->activity =='' || $this->activity == NULL )
     	{
          	$this->activity = '';
     	}
     	if($this->itemsIn =='' || $this->itemsIn == NULL )
     	{
          	$this->itemsIn = 0;
     	}
     	if($this->itemsOut =='' || $this->itemsOut == NULL )
     	{
          	$this->itemsOut = 0;
     	}
     	if($this->adjIn =='' || $this->adjIn == NULL )
     	{
          	$this->adjIn = 0;
     	}
     	if($this->adjOut =='' || $this->adjOut == NULL )
     	{
          	$this->adjOut = 0;
     	}
     	if($this->oldStock =='' || $this->oldStock == NULL )
     	{
          	$this->oldStock = 0;
     	}
     	if($this->currentStock =='' || $this->currentStock == NULL )
     	{
          	$this->currentStock = 0;
     	}
     	if($this->picData =='' || $this->picData == NULL )
     	{
          	$this->picData = '';
     	}
     	if($this->dataTime =='' || $this->dataTime == NULL )
     	{
          	$this->dataTime = '0000-00-00 00:00:00';
     	}
     	
     	$stQuery  = 'UPDATE '.$this->myDb.'.'.$this->myTable.' '; 
     	$stQuery .= 'SET '; 
     	$stQuery .=   'stock_id ="'.$this->db->escape_str($this->stockId).'",'; 
     	$stQuery .=   'items_id ='.$this->db->escape_str($this->itemsId).','; 
     	$stQuery .=   'doc_no ="'.$this->db->escape_str($this->docNo).'",'; 
     	$stQuery .=   'store_id ="'.$this->db->escape_str($this->storeId).'",'; 
     	$stQuery .=   'trn_date ="'.$this->db->escape_str($this->trnDate).'",'; 
     	$stQuery .=   'trn_year ="'.$this->db->escape_str($this->trnYear).'",'; 
     	$stQuery .=   'trn_month ="'.$this->db->escape_str($this->trnMonth).'",'; 
     	$stQuery .=   'activity ="'.$this->db->escape_str($this->activity).'",'; 
     	$stQuery .=   'items_in ='.$this->db->escape_str($this->itemsIn).','; 
     	$stQuery .=   'items_out ='.$this->db->escape_str($this->itemsOut).','; 
     	$stQuery .=   'adj_in ='.$this->db->escape_str($this->adjIn).','; 
     	$stQuery .=   'adj_out ='.$this->db->escape_str($this->adjOut).','; 
     	$stQuery .=   'old_stock ='.$this->db->escape_str($this->oldStock).','; 
     	$stQuery .=   'current_stock ='.$this->db->escape_str($this->currentStock).','; 
     	$stQuery .=   'pic_data ="'.$this->db->escape_str($this->picData).'",'; 
     	$stQuery .=   'data_time ="'.$this->db->escape_str($this->dataTime).'" '; 
     	$stQuery .= 'WHERE '; 
     	$stQuery .=   'stock_id = '.$this->db->escape_str('"'.$id.'"'); 
     	$this->db->query($stQuery); 
     }
     /* END UPDATE */
     /* START DELETE */
     public function delete($id)
     {
     	$stQuery  = 'DELETE FROM '.$this->myDb.'.'.$this->myTable.' '; 
     	$stQuery .= 'WHERE stock_id = '.$this->db->escape_str($id).''; 
     	$this->db->query($stQuery); 
     }
     /* END DELETE */
     /* START GET ALL DATA */
     public function getAll()
     {
     	$this->db->select('*');
     	$this->db->from($this->myDb.'.'.$this->myTable);
     	$this->db->order_by('stock_id', 'ASC');
     	return $this->db->get()->result_array();
     }
     /* END GET ALL DATA */
     /* START GET DATA BY ID */
     public function getById($id)
     {
     	$this->db->select('*');
     	$this->db->from($this->myDb.'.'.$this->myTable);
     	$this->db->where('stock_id', $this->db->escape_str($id));
     	return $this->db->get()->row_array();
     }
     /* END GET DATA BY ID */
     /* START GET OBJECT DATA BY ID */
     public function getObjectById($id)
     {
     	$this->db->select('*');
     	$this->db->from($this->myDb.'.'.$this->myTable);
     	$this->db->where('stock_id', $this->db->escape_str($id));
     	$row = $this->db->get()->row_array();
     	$this->stockId = $row['stock_id']; 
     	$this->itemsId = $row['items_id']; 
     	$this->docNo = $row['doc_no']; 
     	$this->storeId = $row['store_id']; 
     	$this->trnDate = $row['trn_date']; 
     	$this->trnYear = $row['trn_year']; 
     	$this->trnMonth = $row['trn_month']; 
     	$this->activity = $row['activity']; 
     	$this->itemsIn = $row['items_in']; 
     	$this->itemsOut = $row['items_out']; 
     	$this->adjIn = $row['adj_in']; 
     	$this->adjOut = $row['adj_out']; 
     	$this->oldStock = $row['old_stock']; 
     	$this->currentStock = $row['current_stock']; 
     	$this->picData = $row['pic_data']; 
     	$this->dataTime = $row['data_time']; 
     }
     /* END GET OBJECT DATA BY ID */
     /* START OF GET DATA COUNT */
     public function getDataCount()
     {
     	$stQuery  = 'SELECT stock_id FROM '.$this->myDb.'.'.$this->myTable.' '; 
     	$query = $this->db->query($stQuery);
     	return $query->num_rows();
     }
     /* END OF GET DATA COUNT */
     /* START OF RESET VALUES */
     public function resetValues()
     {
     	$this->stockId = ''; 
     	$this->itemsId = 0; 
     	$this->docNo = ''; 
     	$this->storeId = ''; 
     	$this->trnDate = '0000-00-00'; 
     	$this->trnYear = ''; 
     	$this->trnMonth = ''; 
     	$this->activity = ''; 
     	$this->itemsIn = 0; 
     	$this->itemsOut = 0; 
     	$this->adjIn = 0; 
     	$this->adjOut = 0; 
     	$this->oldStock = 0; 
     	$this->currentStock = 0; 
     	$this->picData = ''; 
     	$this->dataTime = '0000-00-00 00:00:00'; 
     }
     /* END OF RESET VALUES */
}
?>
